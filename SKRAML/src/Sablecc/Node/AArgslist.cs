using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AArgslist : PArgslist
	{
		private PArgslist _left_;
		private PArgslist _right_;

		public AArgslist ()
		{
		}

		public AArgslist (
			PArgslist _left_,
			PArgslist _right_
		)
		{
			SetLeft (_left_);
			SetRight (_right_);
		}

		public override object Clone()
		{
			return new AArgslist (
				(PArgslist)CloneNode (_left_),
				(PArgslist)CloneNode (_right_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAArgslist(this);
		}

		public PArgslist GetLeft ()
		{
			return _left_;
		}

		public void SetLeft (PArgslist node)
		{
			_left_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_left_ = node;
		}
		public PArgslist GetRight ()
		{
			return _right_;
		}

		public void SetRight (PArgslist node)
		{
			_right_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_right_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_left_)
			       + ToString (_right_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _left_ == child )
			{
				_left_ = null;
				return;
			}
			if ( _right_ == child )
			{
				_right_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _left_ == oldChild )
			{
				SetLeft ((PArgslist) newChild);
				return;
			}
			if ( _right_ == oldChild )
			{
				SetRight ((PArgslist) newChild);
				return;
			}
		}

	}
}
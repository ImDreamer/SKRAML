using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AArrayvalueidentValue : PValue
	{
		private TIdentifier _identifier_;

		public AArrayvalueidentValue ()
		{
		}

		public AArrayvalueidentValue (
			TIdentifier _identifier_
		)
		{
			SetIdentifier (_identifier_);
		}

		public override object Clone()
		{
			return new AArrayvalueidentValue (
				(TIdentifier)CloneNode (_identifier_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAArrayvalueidentValue(this);
		}

		public TIdentifier GetIdentifier ()
		{
			return _identifier_;
		}

		public void SetIdentifier (TIdentifier node)
		{
			_identifier_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_identifier_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_identifier_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _identifier_ == child )
			{
				_identifier_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _identifier_ == oldChild )
			{
				SetIdentifier ((TIdentifier) newChild);
				return;
			}
		}

	}
}
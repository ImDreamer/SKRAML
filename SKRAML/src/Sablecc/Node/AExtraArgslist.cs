using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AExtraArgslist : PArgslist
	{
		private PArgslist _argslist_;

		public AExtraArgslist ()
		{
		}

		public AExtraArgslist (
			PArgslist _argslist_
		)
		{
			SetArgslist (_argslist_);
		}

		public override object Clone()
		{
			return new AExtraArgslist (
				(PArgslist)CloneNode (_argslist_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAExtraArgslist(this);
		}

		public PArgslist GetArgslist ()
		{
			return _argslist_;
		}

		public void SetArgslist (PArgslist node)
		{
			_argslist_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_argslist_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_argslist_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _argslist_ == child )
			{
				_argslist_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _argslist_ == oldChild )
			{
				SetArgslist ((PArgslist) newChild);
				return;
			}
		}

	}
}
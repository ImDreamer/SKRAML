using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AStringValue : PValue
	{
		private TStringvalue _stringvalue_;

		public AStringValue ()
		{
		}

		public AStringValue (
			TStringvalue _stringvalue_
		)
		{
			SetStringvalue (_stringvalue_);
		}

		public override object Clone()
		{
			return new AStringValue (
				(TStringvalue)CloneNode (_stringvalue_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAStringValue(this);
		}

		public TStringvalue GetStringvalue ()
		{
			return _stringvalue_;
		}

		public void SetStringvalue (TStringvalue node)
		{
			_stringvalue_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_stringvalue_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_stringvalue_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _stringvalue_ == child )
			{
				_stringvalue_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _stringvalue_ == oldChild )
			{
				SetStringvalue ((TStringvalue) newChild);
				return;
			}
		}

	}
}
using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AMethodBlock : PBlock
	{
		private PBlock _block_;
		private PMethod _method_;

		public AMethodBlock ()
		{
		}

		public AMethodBlock (
			PBlock _block_,
			PMethod _method_
		)
		{
			SetBlock (_block_);
			SetMethod (_method_);
		}

		public override object Clone()
		{
			return new AMethodBlock (
				(PBlock)CloneNode (_block_),
				(PMethod)CloneNode (_method_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAMethodBlock(this);
		}

		public PBlock GetBlock ()
		{
			return _block_;
		}

		public void SetBlock (PBlock node)
		{
			_block_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_block_ = node;
		}
		public PMethod GetMethod ()
		{
			return _method_;
		}

		public void SetMethod (PMethod node)
		{
			_method_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_method_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_block_)
			       + ToString (_method_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _block_ == child )
			{
				_block_ = null;
				return;
			}
			if ( _method_ == child )
			{
				_method_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _block_ == oldChild )
			{
				SetBlock ((PBlock) newChild);
				return;
			}
			if ( _method_ == oldChild )
			{
				SetMethod ((PMethod) newChild);
				return;
			}
		}

	}
}
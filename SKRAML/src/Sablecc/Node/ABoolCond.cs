using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class ABoolCond : PCond
	{
		private PValue _value_;
		private PCond _cond_;

		public ABoolCond ()
		{
		}

		public ABoolCond (
			PValue _value_,
			PCond _cond_
		)
		{
			SetValue (_value_);
			SetCond (_cond_);
		}

		public override object Clone()
		{
			return new ABoolCond (
				(PValue)CloneNode (_value_),
				(PCond)CloneNode (_cond_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseABoolCond(this);
		}

		public PValue GetValue ()
		{
			return _value_;
		}

		public void SetValue (PValue node)
		{
			_value_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_value_ = node;
		}
		public PCond GetCond ()
		{
			return _cond_;
		}

		public void SetCond (PCond node)
		{
			_cond_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_cond_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_value_)
			       + ToString (_cond_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _value_ == child )
			{
				_value_ = null;
				return;
			}
			if ( _cond_ == child )
			{
				_cond_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _value_ == oldChild )
			{
				SetValue ((PValue) newChild);
				return;
			}
			if ( _cond_ == oldChild )
			{
				SetCond ((PCond) newChild);
				return;
			}
		}

	}
}
using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class ASinglevalOperation : POperation
	{
		private PValue _value_;

		public ASinglevalOperation ()
		{
		}

		public ASinglevalOperation (
			PValue _value_
		)
		{
			SetValue (_value_);
		}

		public override object Clone()
		{
			return new ASinglevalOperation (
				(PValue)CloneNode (_value_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseASinglevalOperation(this);
		}

		public PValue GetValue ()
		{
			return _value_;
		}

		public void SetValue (PValue node)
		{
			_value_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_value_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_value_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _value_ == child )
			{
				_value_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _value_ == oldChild )
			{
				SetValue ((PValue) newChild);
				return;
			}
		}

	}
}
namespace SKRAML.Sablecc.Node {
	internal interface Cast {
		object Cast(object o); // take ownership
		object UnCast(object o); // release ownership
	}
}
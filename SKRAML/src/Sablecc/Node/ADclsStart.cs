using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class ADclsStart : PStart
	{
		private POperation _operation_;
		private PStart _start_;

		public ADclsStart ()
		{
		}

		public ADclsStart (
			POperation _operation_,
			PStart _start_
		)
		{
			SetOperation (_operation_);
			SetStart (_start_);
		}

		public override object Clone()
		{
			return new ADclsStart (
				(POperation)CloneNode (_operation_),
				(PStart)CloneNode (_start_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseADclsStart(this);
		}

		public POperation GetOperation ()
		{
			return _operation_;
		}

		public void SetOperation (POperation node)
		{
			_operation_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_operation_ = node;
		}
		public PStart GetStart ()
		{
			return _start_;
		}

		public void SetStart (PStart node)
		{
			_start_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_start_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_operation_)
			       + ToString (_start_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _operation_ == child )
			{
				_operation_ = null;
				return;
			}
			if ( _start_ == child )
			{
				_start_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _operation_ == oldChild )
			{
				SetOperation ((POperation) newChild);
				return;
			}
			if ( _start_ == oldChild )
			{
				SetStart ((PStart) newChild);
				return;
			}
		}

	}
}
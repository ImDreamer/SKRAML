using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AStringType : PType
	{
		private TStringkeyword _stringkeyword_;

		public AStringType ()
		{
		}

		public AStringType (
			TStringkeyword _stringkeyword_
		)
		{
			SetStringkeyword (_stringkeyword_);
		}

		public override object Clone()
		{
			return new AStringType (
				(TStringkeyword)CloneNode (_stringkeyword_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAStringType(this);
		}

		public TStringkeyword GetStringkeyword ()
		{
			return _stringkeyword_;
		}

		public void SetStringkeyword (TStringkeyword node)
		{
			_stringkeyword_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_stringkeyword_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_stringkeyword_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _stringkeyword_ == child )
			{
				_stringkeyword_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _stringkeyword_ == oldChild )
			{
				SetStringkeyword ((TStringkeyword) newChild);
				return;
			}
		}

	}
}
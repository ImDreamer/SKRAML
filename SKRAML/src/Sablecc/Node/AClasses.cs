using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AClasses : PClasses
	{
		private TIdentifier _identifier_;
		private PStart _start_;

		public AClasses ()
		{
		}

		public AClasses (
			TIdentifier _identifier_,
			PStart _start_
		)
		{
			SetIdentifier (_identifier_);
			SetStart (_start_);
		}

		public override object Clone()
		{
			return new AClasses (
				(TIdentifier)CloneNode (_identifier_),
				(PStart)CloneNode (_start_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAClasses(this);
		}

		public TIdentifier GetIdentifier ()
		{
			return _identifier_;
		}

		public void SetIdentifier (TIdentifier node)
		{
			_identifier_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_identifier_ = node;
		}
		public PStart GetStart ()
		{
			return _start_;
		}

		public void SetStart (PStart node)
		{
			_start_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_start_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_identifier_)
			       + ToString (_start_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _identifier_ == child )
			{
				_identifier_ = null;
				return;
			}
			if ( _start_ == child )
			{
				_start_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _identifier_ == oldChild )
			{
				SetIdentifier ((TIdentifier) newChild);
				return;
			}
			if ( _start_ == oldChild )
			{
				SetStart ((PStart) newChild);
				return;
			}
		}

	}
}
using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AValrecursiveOperation : POperation
	{
		private POperation _left_;
		private TOp _op_;
		private POperation _right_;

		public AValrecursiveOperation ()
		{
		}

		public AValrecursiveOperation (
			POperation _left_,
			TOp _op_,
			POperation _right_
		)
		{
			SetLeft (_left_);
			SetOp (_op_);
			SetRight (_right_);
		}

		public override object Clone()
		{
			return new AValrecursiveOperation (
				(POperation)CloneNode (_left_),
				(TOp)CloneNode (_op_),
				(POperation)CloneNode (_right_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAValrecursiveOperation(this);
		}

		public POperation GetLeft ()
		{
			return _left_;
		}

		public void SetLeft (POperation node)
		{
			_left_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_left_ = node;
		}
		public TOp GetOp ()
		{
			return _op_;
		}

		public void SetOp (TOp node)
		{
			_op_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_op_ = node;
		}
		public POperation GetRight ()
		{
			return _right_;
		}

		public void SetRight (POperation node)
		{
			_right_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_right_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_left_)
			       + ToString (_op_)
			       + ToString (_right_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _left_ == child )
			{
				_left_ = null;
				return;
			}
			if ( _op_ == child )
			{
				_op_ = null;
				return;
			}
			if ( _right_ == child )
			{
				_right_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _left_ == oldChild )
			{
				SetLeft ((POperation) newChild);
				return;
			}
			if ( _op_ == oldChild )
			{
				SetOp ((TOp) newChild);
				return;
			}
			if ( _right_ == oldChild )
			{
				SetRight ((POperation) newChild);
				return;
			}
		}

	}
}
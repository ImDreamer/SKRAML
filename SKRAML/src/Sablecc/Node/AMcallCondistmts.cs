using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AMcallCondistmts : PCondistmts
	{
		private PMcall _mcall_;
		private PCondistmts _condistmts_;

		public AMcallCondistmts ()
		{
		}

		public AMcallCondistmts (
			PMcall _mcall_,
			PCondistmts _condistmts_
		)
		{
			SetMcall (_mcall_);
			SetCondistmts (_condistmts_);
		}

		public override object Clone()
		{
			return new AMcallCondistmts (
				(PMcall)CloneNode (_mcall_),
				(PCondistmts)CloneNode (_condistmts_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAMcallCondistmts(this);
		}

		public PMcall GetMcall ()
		{
			return _mcall_;
		}

		public void SetMcall (PMcall node)
		{
			_mcall_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_mcall_ = node;
		}
		public PCondistmts GetCondistmts ()
		{
			return _condistmts_;
		}

		public void SetCondistmts (PCondistmts node)
		{
			_condistmts_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_condistmts_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_mcall_)
			       + ToString (_condistmts_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _mcall_ == child )
			{
				_mcall_ = null;
				return;
			}
			if ( _condistmts_ == child )
			{
				_condistmts_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _mcall_ == oldChild )
			{
				SetMcall ((PMcall) newChild);
				return;
			}
			if ( _condistmts_ == oldChild )
			{
				SetCondistmts ((PCondistmts) newChild);
				return;
			}
		}

	}
}
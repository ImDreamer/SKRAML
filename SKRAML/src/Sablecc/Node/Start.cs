using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class Start : Node
	{
		private PStart _base_;
		private EOF _eof_;

		public Start()
		{
		}

		public Start(
			PStart _base_,
			EOF _eof_)
		{
			SetPStart(_base_);
			SetEOF(_eof_);
		}

		public override object Clone()
		{
			return new Start(
				(PStart) CloneNode(_base_),
				(EOF) CloneNode(_eof_));
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseStart(this);
		}

		public PStart GetPStart()
		{
			return _base_;
		}
		public void SetPStart(PStart node)
		{
			_base_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_base_ = node;
		}

		public EOF GetEOF()
		{
			return _eof_;
		}
		public void SetEOF(EOF node)
		{
			_eof_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_eof_ = node;
		}

		internal override void RemoveChild(Node child)
		{
			if(_base_ == child)
			{
				_base_ = null;
				return;
			}

			if(_eof_ == child)
			{
				_eof_ = null;
				return;
			}
		}
		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if(_base_ == oldChild)
			{
				SetPStart((PStart) newChild);
				return;
			}

			if(_eof_ == oldChild)
			{
				SetEOF((EOF) newChild);
				return;
			}
		}

		public override string ToString()
		{
			return "" +
			       ToString(_base_) +
			       ToString(_eof_);
		}
	}
}
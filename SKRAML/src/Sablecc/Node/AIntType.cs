using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AIntType : PType
	{
		private TIntkeyword _intkeyword_;

		public AIntType ()
		{
		}

		public AIntType (
			TIntkeyword _intkeyword_
		)
		{
			SetIntkeyword (_intkeyword_);
		}

		public override object Clone()
		{
			return new AIntType (
				(TIntkeyword)CloneNode (_intkeyword_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAIntType(this);
		}

		public TIntkeyword GetIntkeyword ()
		{
			return _intkeyword_;
		}

		public void SetIntkeyword (TIntkeyword node)
		{
			_intkeyword_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_intkeyword_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_intkeyword_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _intkeyword_ == child )
			{
				_intkeyword_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _intkeyword_ == oldChild )
			{
				SetIntkeyword ((TIntkeyword) newChild);
				return;
			}
		}

	}
}
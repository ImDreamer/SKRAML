using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AValupsingOperation : POperation
	{
		private POperation _operation_;

		public AValupsingOperation ()
		{
		}

		public AValupsingOperation (
			POperation _operation_
		)
		{
			SetOperation (_operation_);
		}

		public override object Clone()
		{
			return new AValupsingOperation (
				(POperation)CloneNode (_operation_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAValupsingOperation(this);
		}

		public POperation GetOperation ()
		{
			return _operation_;
		}

		public void SetOperation (POperation node)
		{
			_operation_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_operation_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_operation_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _operation_ == child )
			{
				_operation_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _operation_ == oldChild )
			{
				SetOperation ((POperation) newChild);
				return;
			}
		}

	}
}
using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AArrayassOperation : POperation
	{
		private TIdentifier _identifier_;
		private PValue _value_;
		private POperation _operation_;

		public AArrayassOperation ()
		{
		}

		public AArrayassOperation (
			TIdentifier _identifier_,
			PValue _value_,
			POperation _operation_
		)
		{
			SetIdentifier (_identifier_);
			SetValue (_value_);
			SetOperation (_operation_);
		}

		public override object Clone()
		{
			return new AArrayassOperation (
				(TIdentifier)CloneNode (_identifier_),
				(PValue)CloneNode (_value_),
				(POperation)CloneNode (_operation_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAArrayassOperation(this);
		}

		public TIdentifier GetIdentifier ()
		{
			return _identifier_;
		}

		public void SetIdentifier (TIdentifier node)
		{
			_identifier_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_identifier_ = node;
		}
		public PValue GetValue ()
		{
			return _value_;
		}

		public void SetValue (PValue node)
		{
			_value_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_value_ = node;
		}
		public POperation GetOperation ()
		{
			return _operation_;
		}

		public void SetOperation (POperation node)
		{
			_operation_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_operation_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_identifier_)
			       + ToString (_value_)
			       + ToString (_operation_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _identifier_ == child )
			{
				_identifier_ = null;
				return;
			}
			if ( _value_ == child )
			{
				_value_ = null;
				return;
			}
			if ( _operation_ == child )
			{
				_operation_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _identifier_ == oldChild )
			{
				SetIdentifier ((TIdentifier) newChild);
				return;
			}
			if ( _value_ == oldChild )
			{
				SetValue ((PValue) newChild);
				return;
			}
			if ( _operation_ == oldChild )
			{
				SetOperation ((POperation) newChild);
				return;
			}
		}

	}
}
using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AStmtsBlock : PBlock
	{
		private PBlock _block_;
		private PStmt _stmt_;

		public AStmtsBlock ()
		{
		}

		public AStmtsBlock (
			PBlock _block_,
			PStmt _stmt_
		)
		{
			SetBlock (_block_);
			SetStmt (_stmt_);
		}

		public override object Clone()
		{
			return new AStmtsBlock (
				(PBlock)CloneNode (_block_),
				(PStmt)CloneNode (_stmt_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAStmtsBlock(this);
		}

		public PBlock GetBlock ()
		{
			return _block_;
		}

		public void SetBlock (PBlock node)
		{
			_block_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_block_ = node;
		}
		public PStmt GetStmt ()
		{
			return _stmt_;
		}

		public void SetStmt (PStmt node)
		{
			_stmt_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_stmt_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_block_)
			       + ToString (_stmt_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _block_ == child )
			{
				_block_ = null;
				return;
			}
			if ( _stmt_ == child )
			{
				_stmt_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _block_ == oldChild )
			{
				SetBlock ((PBlock) newChild);
				return;
			}
			if ( _stmt_ == oldChild )
			{
				SetStmt ((PStmt) newChild);
				return;
			}
		}

	}
}
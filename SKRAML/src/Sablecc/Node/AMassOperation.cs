using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AMassOperation : POperation
	{
		private PMcall _mcall_;
		private POperation _operation_;

		public AMassOperation ()
		{
		}

		public AMassOperation (
			PMcall _mcall_,
			POperation _operation_
		)
		{
			SetMcall (_mcall_);
			SetOperation (_operation_);
		}

		public override object Clone()
		{
			return new AMassOperation (
				(PMcall)CloneNode (_mcall_),
				(POperation)CloneNode (_operation_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAMassOperation(this);
		}

		public PMcall GetMcall ()
		{
			return _mcall_;
		}

		public void SetMcall (PMcall node)
		{
			_mcall_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_mcall_ = node;
		}
		public POperation GetOperation ()
		{
			return _operation_;
		}

		public void SetOperation (POperation node)
		{
			_operation_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_operation_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_mcall_)
			       + ToString (_operation_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _mcall_ == child )
			{
				_mcall_ = null;
				return;
			}
			if ( _operation_ == child )
			{
				_operation_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _mcall_ == oldChild )
			{
				SetMcall ((PMcall) newChild);
				return;
			}
			if ( _operation_ == oldChild )
			{
				SetOperation ((POperation) newChild);
				return;
			}
		}

	}
}
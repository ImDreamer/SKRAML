using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AValurecOperation : POperation
	{
		private POperation _operation_;
		private TUpperops _upperops_;
		private PValue _value_;

		public AValurecOperation ()
		{
		}

		public AValurecOperation (
			POperation _operation_,
			TUpperops _upperops_,
			PValue _value_
		)
		{
			SetOperation (_operation_);
			SetUpperops (_upperops_);
			SetValue (_value_);
		}

		public override object Clone()
		{
			return new AValurecOperation (
				(POperation)CloneNode (_operation_),
				(TUpperops)CloneNode (_upperops_),
				(PValue)CloneNode (_value_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAValurecOperation(this);
		}

		public POperation GetOperation ()
		{
			return _operation_;
		}

		public void SetOperation (POperation node)
		{
			_operation_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_operation_ = node;
		}
		public TUpperops GetUpperops ()
		{
			return _upperops_;
		}

		public void SetUpperops (TUpperops node)
		{
			_upperops_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_upperops_ = node;
		}
		public PValue GetValue ()
		{
			return _value_;
		}

		public void SetValue (PValue node)
		{
			_value_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_value_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_operation_)
			       + ToString (_upperops_)
			       + ToString (_value_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _operation_ == child )
			{
				_operation_ = null;
				return;
			}
			if ( _upperops_ == child )
			{
				_upperops_ = null;
				return;
			}
			if ( _value_ == child )
			{
				_value_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _operation_ == oldChild )
			{
				SetOperation ((POperation) newChild);
				return;
			}
			if ( _upperops_ == oldChild )
			{
				SetUpperops ((TUpperops) newChild);
				return;
			}
			if ( _value_ == oldChild )
			{
				SetValue ((PValue) newChild);
				return;
			}
		}

	}
}
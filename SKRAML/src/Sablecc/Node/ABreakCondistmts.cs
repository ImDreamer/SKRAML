using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class ABreakCondistmts : PCondistmts
	{


		public ABreakCondistmts (
		)
		{
		}

		public override object Clone()
		{
			return new ABreakCondistmts (
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseABreakCondistmts(this);
		}


		public override string ToString()
		{
			return ""
				;
		}

		internal override void RemoveChild(Node child)
		{
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
		}

	}
}
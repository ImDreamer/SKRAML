using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class ADclStart : PStart
	{
		private PDcl _dcl_;
		private PStart _start_;

		public ADclStart ()
		{
		}

		public ADclStart (
			PDcl _dcl_,
			PStart _start_
		)
		{
			SetDcl (_dcl_);
			SetStart (_start_);
		}

		public override object Clone()
		{
			return new ADclStart (
				(PDcl)CloneNode (_dcl_),
				(PStart)CloneNode (_start_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseADclStart(this);
		}

		public PDcl GetDcl ()
		{
			return _dcl_;
		}

		public void SetDcl (PDcl node)
		{
			_dcl_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_dcl_ = node;
		}
		public PStart GetStart ()
		{
			return _start_;
		}

		public void SetStart (PStart node)
		{
			_start_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_start_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_dcl_)
			       + ToString (_start_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _dcl_ == child )
			{
				_dcl_ = null;
				return;
			}
			if ( _start_ == child )
			{
				_start_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _dcl_ == oldChild )
			{
				SetDcl ((PDcl) newChild);
				return;
			}
			if ( _start_ == oldChild )
			{
				SetStart ((PStart) newChild);
				return;
			}
		}

	}
}
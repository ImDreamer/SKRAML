using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AElseCondiblck : PCondiblck
	{
		private PScope _open_;
		private PBlock _block_;
		private PCondistmts _loopmod_;
		private PScope _close_;

		public AElseCondiblck ()
		{
		}

		public AElseCondiblck (
			PScope _open_,
			PBlock _block_,
			PCondistmts _loopmod_,
			PScope _close_
		)
		{
			SetOpen (_open_);
			SetBlock (_block_);
			SetLoopmod (_loopmod_);
			SetClose (_close_);
		}

		public override object Clone()
		{
			return new AElseCondiblck (
				(PScope)CloneNode (_open_),
				(PBlock)CloneNode (_block_),
				(PCondistmts)CloneNode (_loopmod_),
				(PScope)CloneNode (_close_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAElseCondiblck(this);
		}

		public PScope GetOpen ()
		{
			return _open_;
		}

		public void SetOpen (PScope node)
		{
			_open_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_open_ = node;
		}
		public PBlock GetBlock ()
		{
			return _block_;
		}

		public void SetBlock (PBlock node)
		{
			_block_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_block_ = node;
		}
		public PCondistmts GetLoopmod ()
		{
			return _loopmod_;
		}

		public void SetLoopmod (PCondistmts node)
		{
			_loopmod_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_loopmod_ = node;
		}
		public PScope GetClose ()
		{
			return _close_;
		}

		public void SetClose (PScope node)
		{
			_close_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_close_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_open_)
			       + ToString (_block_)
			       + ToString (_loopmod_)
			       + ToString (_close_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _open_ == child )
			{
				_open_ = null;
				return;
			}
			if ( _block_ == child )
			{
				_block_ = null;
				return;
			}
			if ( _loopmod_ == child )
			{
				_loopmod_ = null;
				return;
			}
			if ( _close_ == child )
			{
				_close_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _open_ == oldChild )
			{
				SetOpen ((PScope) newChild);
				return;
			}
			if ( _block_ == oldChild )
			{
				SetBlock ((PBlock) newChild);
				return;
			}
			if ( _loopmod_ == oldChild )
			{
				SetLoopmod ((PCondistmts) newChild);
				return;
			}
			if ( _close_ == oldChild )
			{
				SetClose ((PScope) newChild);
				return;
			}
		}

	}
}
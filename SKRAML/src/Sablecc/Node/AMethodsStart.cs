using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AMethodsStart : PStart
	{
		private PMethod _method_;
		private PStart _start_;

		public AMethodsStart ()
		{
		}

		public AMethodsStart (
			PMethod _method_,
			PStart _start_
		)
		{
			SetMethod (_method_);
			SetStart (_start_);
		}

		public override object Clone()
		{
			return new AMethodsStart (
				(PMethod)CloneNode (_method_),
				(PStart)CloneNode (_start_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAMethodsStart(this);
		}

		public PMethod GetMethod ()
		{
			return _method_;
		}

		public void SetMethod (PMethod node)
		{
			_method_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_method_ = node;
		}
		public PStart GetStart ()
		{
			return _start_;
		}

		public void SetStart (PStart node)
		{
			_start_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_start_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_method_)
			       + ToString (_start_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _method_ == child )
			{
				_method_ = null;
				return;
			}
			if ( _start_ == child )
			{
				_start_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _method_ == oldChild )
			{
				SetMethod ((PMethod) newChild);
				return;
			}
			if ( _start_ == oldChild )
			{
				SetStart ((PStart) newChild);
				return;
			}
		}

	}
}
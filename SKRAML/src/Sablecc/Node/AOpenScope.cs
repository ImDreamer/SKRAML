using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AOpenScope : PScope
	{


		public AOpenScope (
		)
		{
		}

		public override object Clone()
		{
			return new AOpenScope (
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAOpenScope(this);
		}


		public override string ToString()
		{
			return ""
				;
		}

		internal override void RemoveChild(Node child)
		{
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
		}

	}
}
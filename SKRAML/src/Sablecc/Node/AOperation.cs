using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AOperation : POperation {
		private PType _type_;
		private TIdentifier _identifier_;
		private POperation _operation_;

		public AOperation() { }

		public AOperation(
			PType _type_,
			TIdentifier _identifier_,
			POperation _operation_
		) {
			SetType(_type_);
			SetIdentifier(_identifier_);
			SetOperation(_operation_);
		}

		public override object Clone() {
			return new AOperation(
				(PType) CloneNode(_type_),
				(TIdentifier) CloneNode(_identifier_),
				(POperation) CloneNode(_operation_)
			);
		}

		public override void Apply(Switch sw) {
			((IAnalysis) sw).CaseAOperation(this);
		}

		public new PType GetType() {
			return _type_;
		}

		public void SetType(PType node) {
			_type_?.Parent(null);

			if (node != null) {
				if (node.Parent() != null) {
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_type_ = node;
		}

		public TIdentifier GetIdentifier() {
			return _identifier_;
		}

		public void SetIdentifier(TIdentifier node) {
			_identifier_?.Parent(null);

			if (node != null) {
				if (node.Parent() != null) {
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_identifier_ = node;
		}

		public POperation GetOperation() {
			return _operation_;
		}

		public void SetOperation(POperation node) {
			_operation_?.Parent(null);

			if (node != null) {
				if (node.Parent() != null) {
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_operation_ = node;
		}

		public override string ToString() {
			return ""
			       + ToString(_type_)
			       + ToString(_identifier_)
			       + ToString(_operation_)
				;
		}

		internal override void RemoveChild(Node child) {
			if (_type_ == child) {
				_type_ = null;
				return;
			}

			if (_identifier_ == child) {
				_identifier_ = null;
				return;
			}

			if (_operation_ == child) {
				_operation_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild) {
			if (_type_ == oldChild) {
				SetType((PType) newChild);
				return;
			}

			if (_identifier_ == oldChild) {
				SetIdentifier((TIdentifier) newChild);
				return;
			}

			if (_operation_ == oldChild) {
				SetOperation((POperation) newChild);
				return;
			}
		}
	}
}
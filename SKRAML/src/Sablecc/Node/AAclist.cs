using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AAclist : PAclist
	{
		private PAclist _left_;
		private PAclist _right_;

		public AAclist ()
		{
		}

		public AAclist (
			PAclist _left_,
			PAclist _right_
		)
		{
			SetLeft (_left_);
			SetRight (_right_);
		}

		public override object Clone()
		{
			return new AAclist (
				(PAclist)CloneNode (_left_),
				(PAclist)CloneNode (_right_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAAclist(this);
		}

		public PAclist GetLeft ()
		{
			return _left_;
		}

		public void SetLeft (PAclist node)
		{
			_left_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_left_ = node;
		}
		public PAclist GetRight ()
		{
			return _right_;
		}

		public void SetRight (PAclist node)
		{
			_right_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_right_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_left_)
			       + ToString (_right_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _left_ == child )
			{
				_left_ = null;
				return;
			}
			if ( _right_ == child )
			{
				_right_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _left_ == oldChild )
			{
				SetLeft ((PAclist) newChild);
				return;
			}
			if ( _right_ == oldChild )
			{
				SetRight ((PAclist) newChild);
				return;
			}
		}

	}
}
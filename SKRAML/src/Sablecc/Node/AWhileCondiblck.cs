using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AWhileCondiblck : PCondiblck
	{
		private PScope _open_;
		private PCond _cond_;
		private PCondistmts _condistmts_;
		private PScope _close_;

		public AWhileCondiblck ()
		{
		}

		public AWhileCondiblck (
			PScope _open_,
			PCond _cond_,
			PCondistmts _condistmts_,
			PScope _close_
		)
		{
			SetOpen (_open_);
			SetCond (_cond_);
			SetCondistmts (_condistmts_);
			SetClose (_close_);
		}

		public override object Clone()
		{
			return new AWhileCondiblck (
				(PScope)CloneNode (_open_),
				(PCond)CloneNode (_cond_),
				(PCondistmts)CloneNode (_condistmts_),
				(PScope)CloneNode (_close_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAWhileCondiblck(this);
		}

		public PScope GetOpen ()
		{
			return _open_;
		}

		public void SetOpen (PScope node)
		{
			_open_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_open_ = node;
		}
		public PCond GetCond ()
		{
			return _cond_;
		}

		public void SetCond (PCond node)
		{
			_cond_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_cond_ = node;
		}
		public PCondistmts GetCondistmts ()
		{
			return _condistmts_;
		}

		public void SetCondistmts (PCondistmts node)
		{
			_condistmts_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_condistmts_ = node;
		}
		public PScope GetClose ()
		{
			return _close_;
		}

		public void SetClose (PScope node)
		{
			_close_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_close_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_open_)
			       + ToString (_cond_)
			       + ToString (_condistmts_)
			       + ToString (_close_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _open_ == child )
			{
				_open_ = null;
				return;
			}
			if ( _cond_ == child )
			{
				_cond_ = null;
				return;
			}
			if ( _condistmts_ == child )
			{
				_condistmts_ = null;
				return;
			}
			if ( _close_ == child )
			{
				_close_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _open_ == oldChild )
			{
				SetOpen ((PScope) newChild);
				return;
			}
			if ( _cond_ == oldChild )
			{
				SetCond ((PCond) newChild);
				return;
			}
			if ( _condistmts_ == oldChild )
			{
				SetCondistmts ((PCondistmts) newChild);
				return;
			}
			if ( _close_ == oldChild )
			{
				SetClose ((PScope) newChild);
				return;
			}
		}

	}
}
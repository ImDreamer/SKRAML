using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class ACharType : PType
	{
		private TCharkeyword _charkeyword_;

		public ACharType ()
		{
		}

		public ACharType (
			TCharkeyword _charkeyword_
		)
		{
			SetCharkeyword (_charkeyword_);
		}

		public override object Clone()
		{
			return new ACharType (
				(TCharkeyword)CloneNode (_charkeyword_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseACharType(this);
		}

		public TCharkeyword GetCharkeyword ()
		{
			return _charkeyword_;
		}

		public void SetCharkeyword (TCharkeyword node)
		{
			_charkeyword_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_charkeyword_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_charkeyword_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _charkeyword_ == child )
			{
				_charkeyword_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _charkeyword_ == oldChild )
			{
				SetCharkeyword ((TCharkeyword) newChild);
				return;
			}
		}

	}
}
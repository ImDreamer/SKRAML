using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class ADclCondistmts : PCondistmts
	{
		private PDcl _dcl_;
		private PCondistmts _condistmts_;

		public ADclCondistmts ()
		{
		}

		public ADclCondistmts (
			PDcl _dcl_,
			PCondistmts _condistmts_
		)
		{
			SetDcl (_dcl_);
			SetCondistmts (_condistmts_);
		}

		public override object Clone()
		{
			return new ADclCondistmts (
				(PDcl)CloneNode (_dcl_),
				(PCondistmts)CloneNode (_condistmts_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseADclCondistmts(this);
		}

		public PDcl GetDcl ()
		{
			return _dcl_;
		}

		public void SetDcl (PDcl node)
		{
			_dcl_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_dcl_ = node;
		}
		public PCondistmts GetCondistmts ()
		{
			return _condistmts_;
		}

		public void SetCondistmts (PCondistmts node)
		{
			_condistmts_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_condistmts_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_dcl_)
			       + ToString (_condistmts_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _dcl_ == child )
			{
				_dcl_ = null;
				return;
			}
			if ( _condistmts_ == child )
			{
				_condistmts_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _dcl_ == oldChild )
			{
				SetDcl ((PDcl) newChild);
				return;
			}
			if ( _condistmts_ == oldChild )
			{
				SetCondistmts ((PCondistmts) newChild);
				return;
			}
		}

	}
}
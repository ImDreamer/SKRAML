using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AVoidType : PType
	{
		private TVoidkeyword _voidkeyword_;

		public AVoidType ()
		{
		}

		public AVoidType (
			TVoidkeyword _voidkeyword_
		)
		{
			SetVoidkeyword (_voidkeyword_);
		}

		public override object Clone()
		{
			return new AVoidType (
				(TVoidkeyword)CloneNode (_voidkeyword_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAVoidType(this);
		}

		public TVoidkeyword GetVoidkeyword ()
		{
			return _voidkeyword_;
		}

		public void SetVoidkeyword (TVoidkeyword node)
		{
			_voidkeyword_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_voidkeyword_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_voidkeyword_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _voidkeyword_ == child )
			{
				_voidkeyword_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _voidkeyword_ == oldChild )
			{
				SetVoidkeyword ((TVoidkeyword) newChild);
				return;
			}
		}

	}
}
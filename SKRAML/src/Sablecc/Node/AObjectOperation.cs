using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AObjectOperation : POperation
	{
		private PType _type_;
		private TIdentifier _name_;

		public AObjectOperation ()
		{
		}

		public AObjectOperation (
			PType _type_,
			TIdentifier _name_
		)
		{
			SetType (_type_);
			SetName (_name_);
		}

		public override object Clone()
		{
			return new AObjectOperation (
				(PType)CloneNode (_type_),
				(TIdentifier)CloneNode (_name_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAObjectOperation(this);
		}

		public new PType GetType ()
		{
			return _type_;
		}

		public void SetType (PType node)
		{
			_type_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_type_ = node;
		}
		public TIdentifier GetName ()
		{
			return _name_;
		}

		public void SetName (TIdentifier node)
		{
			_name_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_name_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_type_)
			       + ToString (_name_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _type_ == child )
			{
				_type_ = null;
				return;
			}
			if ( _name_ == child )
			{
				_name_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _type_ == oldChild )
			{
				SetType ((PType) newChild);
				return;
			}
			if ( _name_ == oldChild )
			{
				SetName ((TIdentifier) newChild);
				return;
			}
		}

	}
}
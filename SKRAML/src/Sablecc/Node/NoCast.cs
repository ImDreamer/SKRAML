namespace SKRAML.Sablecc.Node {
	internal class NoCast : Cast {
		private static NoCast instance = new NoCast();

		public static NoCast Instance {
			get { return instance; }
		}

		private NoCast() { }

		public object Cast(object o) {
			return o;
		}

		public object UnCast(object o) {
			return o;
		}
	}
}
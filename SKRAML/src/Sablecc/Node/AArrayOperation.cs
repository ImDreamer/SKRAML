using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AArrayOperation : POperation
	{
		private PType _type_;
		private TIdentifier _identifier_;
		private PValue _value_;

		public AArrayOperation ()
		{
		}

		public AArrayOperation (
			PType _type_,
			TIdentifier _identifier_,
			PValue _value_
		)
		{
			SetType (_type_);
			SetIdentifier (_identifier_);
			SetValue (_value_);
		}

		public override object Clone()
		{
			return new AArrayOperation (
				(PType)CloneNode (_type_),
				(TIdentifier)CloneNode (_identifier_),
				(PValue)CloneNode (_value_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAArrayOperation(this);
		}

		public new PType GetType ()
		{
			return _type_;
		}

		public void SetType (PType node)
		{
			_type_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_type_ = node;
		}
		public TIdentifier GetIdentifier ()
		{
			return _identifier_;
		}

		public void SetIdentifier (TIdentifier node)
		{
			_identifier_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_identifier_ = node;
		}
		public PValue GetValue ()
		{
			return _value_;
		}

		public void SetValue (PValue node)
		{
			_value_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_value_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_type_)
			       + ToString (_identifier_)
			       + ToString (_value_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _type_ == child )
			{
				_type_ = null;
				return;
			}
			if ( _identifier_ == child )
			{
				_identifier_ = null;
				return;
			}
			if ( _value_ == child )
			{
				_value_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _type_ == oldChild )
			{
				SetType ((PType) newChild);
				return;
			}
			if ( _identifier_ == oldChild )
			{
				SetIdentifier ((TIdentifier) newChild);
				return;
			}
			if ( _value_ == oldChild )
			{
				SetValue ((PValue) newChild);
				return;
			}
		}

	}
}
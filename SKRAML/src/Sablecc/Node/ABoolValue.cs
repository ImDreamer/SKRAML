using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class ABoolValue : PValue
	{
		private TBoolvalue _boolvalue_;

		public ABoolValue ()
		{
		}

		public ABoolValue (
			TBoolvalue _boolvalue_
		)
		{
			SetBoolvalue (_boolvalue_);
		}

		public override object Clone()
		{
			return new ABoolValue (
				(TBoolvalue)CloneNode (_boolvalue_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseABoolValue(this);
		}

		public TBoolvalue GetBoolvalue ()
		{
			return _boolvalue_;
		}

		public void SetBoolvalue (TBoolvalue node)
		{
			_boolvalue_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_boolvalue_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_boolvalue_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _boolvalue_ == child )
			{
				_boolvalue_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _boolvalue_ == oldChild )
			{
				SetBoolvalue ((TBoolvalue) newChild);
				return;
			}
		}

	}
}
using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AEofStart : PStart
	{


		public AEofStart (
		)
		{
		}

		public override object Clone()
		{
			return new AEofStart (
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAEofStart(this);
		}


		public override string ToString()
		{
			return ""
				;
		}

		internal override void RemoveChild(Node child)
		{
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
		}

	}
}
/* This file was generated by SableCC (http://www.sablecc.org/). */

using System;
using System.Collections;
using System.Text;

namespace SKRAML.Sablecc.Node {
	public abstract class Node : Switchable, ICloneable {
		private Node parent_node;

		public abstract object Clone();
		public abstract void Apply(Switch s);

		public Node Parent() {
			return parent_node;
		}

		internal void Parent(Node parent_node) {
			this.parent_node = parent_node;
		}

		internal abstract void RemoveChild(Node child);
		internal abstract void ReplaceChild(Node oldChild, Node newChild);

		public void ReplaceBy(Node node) {
			parent_node?.ReplaceChild(this, node);
		}

		protected string ToString(Node node) {
			if (node != null) {
				return node.ToString();
			}

			return "";
		}

		protected string ToString(IList list) {
			StringBuilder s = new StringBuilder();

			foreach (Node n in list) {
				s.Append(n.ToString());
			}

			return s.ToString();
		}

		protected Node CloneNode(Node node) {
			return (Node) node?.Clone();
		}

		protected IList CloneList(IList list) {
			// list not owned by anyone so no need for TypedList
			IList clone = new ArrayList();

			foreach (Node n in list) {
				clone.Add(n.Clone());
			}

			return clone;
		}
	}
} // namespace skraml.sablecc.node
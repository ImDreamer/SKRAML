using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class ACond : PCond
	{
		private PValue _left_;
		private TCompop _compop_;
		private PValue _right_;
		private PCond _cond_;

		public ACond ()
		{
		}

		public ACond (
			PValue _left_,
			TCompop _compop_,
			PValue _right_,
			PCond _cond_
		)
		{
			SetLeft (_left_);
			SetCompop (_compop_);
			SetRight (_right_);
			SetCond (_cond_);
		}

		public override object Clone()
		{
			return new ACond (
				(PValue)CloneNode (_left_),
				(TCompop)CloneNode (_compop_),
				(PValue)CloneNode (_right_),
				(PCond)CloneNode (_cond_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseACond(this);
		}

		public PValue GetLeft ()
		{
			return _left_;
		}

		public void SetLeft (PValue node)
		{
			_left_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_left_ = node;
		}
		public TCompop GetCompop ()
		{
			return _compop_;
		}

		public void SetCompop (TCompop node)
		{
			_compop_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_compop_ = node;
		}
		public PValue GetRight ()
		{
			return _right_;
		}

		public void SetRight (PValue node)
		{
			_right_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_right_ = node;
		}
		public PCond GetCond ()
		{
			return _cond_;
		}

		public void SetCond (PCond node)
		{
			_cond_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_cond_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_left_)
			       + ToString (_compop_)
			       + ToString (_right_)
			       + ToString (_cond_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _left_ == child )
			{
				_left_ = null;
				return;
			}
			if ( _compop_ == child )
			{
				_compop_ = null;
				return;
			}
			if ( _right_ == child )
			{
				_right_ = null;
				return;
			}
			if ( _cond_ == child )
			{
				_cond_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _left_ == oldChild )
			{
				SetLeft ((PValue) newChild);
				return;
			}
			if ( _compop_ == oldChild )
			{
				SetCompop ((TCompop) newChild);
				return;
			}
			if ( _right_ == oldChild )
			{
				SetRight ((PValue) newChild);
				return;
			}
			if ( _cond_ == oldChild )
			{
				SetCond ((PCond) newChild);
				return;
			}
		}

	}
}
namespace SKRAML.Sablecc.Node {
	public interface Switchable {
		void Apply(Switch sw);
	}
}
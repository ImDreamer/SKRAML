using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AEndofwhileCondistmts : PCondistmts
	{


		public AEndofwhileCondistmts (
		)
		{
		}

		public override object Clone()
		{
			return new AEndofwhileCondistmts (
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAEndofwhileCondistmts(this);
		}


		public override string ToString()
		{
			return ""
				;
		}

		internal override void RemoveChild(Node child)
		{
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
		}

	}
}
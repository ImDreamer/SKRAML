namespace SKRAML.Sablecc.Node {
	internal class NodeCast : Cast {
		private static NodeCast instance = new NodeCast();

		public static NodeCast Instance {
			get { return instance; }
		}

		private NodeCast() { }

		public object Cast(object o) {
			return (Node) o;
		}

		public object UnCast(object o) {
			return o;
		}
	}
}
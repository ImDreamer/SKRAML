using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AMethod : PMethod
	{
		private PType _type_;
		private TIdentifier _identifier_;
		private PArgslist _argslist_;
		private PBlock _block_;

		public AMethod ()
		{
		}

		public AMethod (
			PType _type_,
			TIdentifier _identifier_,
			PArgslist _argslist_,
			PBlock _block_
		)
		{
			SetType (_type_);
			SetIdentifier (_identifier_);
			SetArgslist (_argslist_);
			SetBlock (_block_);
		}

		public override object Clone()
		{
			return new AMethod (
				(PType)CloneNode (_type_),
				(TIdentifier)CloneNode (_identifier_),
				(PArgslist)CloneNode (_argslist_),
				(PBlock)CloneNode (_block_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAMethod(this);
		}

		public new PType GetType ()
		{
			return _type_;
		}

		public void SetType (PType node)
		{
			_type_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_type_ = node;
		}
		public TIdentifier GetIdentifier ()
		{
			return _identifier_;
		}

		public void SetIdentifier (TIdentifier node)
		{
			_identifier_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_identifier_ = node;
		}
		public PArgslist GetArgslist ()
		{
			return _argslist_;
		}

		public void SetArgslist (PArgslist node)
		{
			_argslist_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_argslist_ = node;
		}
		public PBlock GetBlock ()
		{
			return _block_;
		}

		public void SetBlock (PBlock node)
		{
			_block_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_block_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_type_)
			       + ToString (_identifier_)
			       + ToString (_argslist_)
			       + ToString (_block_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _type_ == child )
			{
				_type_ = null;
				return;
			}
			if ( _identifier_ == child )
			{
				_identifier_ = null;
				return;
			}
			if ( _argslist_ == child )
			{
				_argslist_ = null;
				return;
			}
			if ( _block_ == child )
			{
				_block_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _type_ == oldChild )
			{
				SetType ((PType) newChild);
				return;
			}
			if ( _identifier_ == oldChild )
			{
				SetIdentifier ((TIdentifier) newChild);
				return;
			}
			if ( _argslist_ == oldChild )
			{
				SetArgslist ((PArgslist) newChild);
				return;
			}
			if ( _block_ == oldChild )
			{
				SetBlock ((PBlock) newChild);
				return;
			}
		}

	}
}
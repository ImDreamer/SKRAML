using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class ADclBlock : PBlock
	{
		private PBlock _block_;
		private PDcl _dcl_;

		public ADclBlock ()
		{
		}

		public ADclBlock (
			PBlock _block_,
			PDcl _dcl_
		)
		{
			SetBlock (_block_);
			SetDcl (_dcl_);
		}

		public override object Clone()
		{
			return new ADclBlock (
				(PBlock)CloneNode (_block_),
				(PDcl)CloneNode (_dcl_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseADclBlock(this);
		}

		public PBlock GetBlock ()
		{
			return _block_;
		}

		public void SetBlock (PBlock node)
		{
			_block_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_block_ = node;
		}
		public PDcl GetDcl ()
		{
			return _dcl_;
		}

		public void SetDcl (PDcl node)
		{
			_dcl_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_dcl_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_block_)
			       + ToString (_dcl_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _block_ == child )
			{
				_block_ = null;
				return;
			}
			if ( _dcl_ == child )
			{
				_dcl_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _block_ == oldChild )
			{
				SetBlock ((PBlock) newChild);
				return;
			}
			if ( _dcl_ == oldChild )
			{
				SetDcl ((PDcl) newChild);
				return;
			}
		}

	}
}
using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AMcall : PMcall
	{
		private TIdentifier _identifier_;
		private PAclist _aclist_;
		private PMcall _mcall_;

		public AMcall ()
		{
		}

		public AMcall (
			TIdentifier _identifier_,
			PAclist _aclist_,
			PMcall _mcall_
		)
		{
			SetIdentifier (_identifier_);
			SetAclist (_aclist_);
			SetMcall (_mcall_);
		}

		public override object Clone()
		{
			return new AMcall (
				(TIdentifier)CloneNode (_identifier_),
				(PAclist)CloneNode (_aclist_),
				(PMcall)CloneNode (_mcall_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAMcall(this);
		}

		public TIdentifier GetIdentifier ()
		{
			return _identifier_;
		}

		public void SetIdentifier (TIdentifier node)
		{
			_identifier_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_identifier_ = node;
		}
		public PAclist GetAclist ()
		{
			return _aclist_;
		}

		public void SetAclist (PAclist node)
		{
			_aclist_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_aclist_ = node;
		}
		public PMcall GetMcall ()
		{
			return _mcall_;
		}

		public void SetMcall (PMcall node)
		{
			_mcall_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_mcall_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_identifier_)
			       + ToString (_aclist_)
			       + ToString (_mcall_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _identifier_ == child )
			{
				_identifier_ = null;
				return;
			}
			if ( _aclist_ == child )
			{
				_aclist_ = null;
				return;
			}
			if ( _mcall_ == child )
			{
				_mcall_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _identifier_ == oldChild )
			{
				SetIdentifier ((TIdentifier) newChild);
				return;
			}
			if ( _aclist_ == oldChild )
			{
				SetAclist ((PAclist) newChild);
				return;
			}
			if ( _mcall_ == oldChild )
			{
				SetMcall ((PMcall) newChild);
				return;
			}
		}

	}
}
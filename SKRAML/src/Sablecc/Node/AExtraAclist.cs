using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AExtraAclist : PAclist
	{
		private PAclist _aclist_;

		public AExtraAclist ()
		{
		}

		public AExtraAclist (
			PAclist _aclist_
		)
		{
			SetAclist (_aclist_);
		}

		public override object Clone()
		{
			return new AExtraAclist (
				(PAclist)CloneNode (_aclist_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAExtraAclist(this);
		}

		public PAclist GetAclist ()
		{
			return _aclist_;
		}

		public void SetAclist (PAclist node)
		{
			_aclist_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_aclist_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_aclist_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _aclist_ == child )
			{
				_aclist_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _aclist_ == oldChild )
			{
				SetAclist ((PAclist) newChild);
				return;
			}
		}

	}
}
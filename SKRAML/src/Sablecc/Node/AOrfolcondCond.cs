using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AOrfolcondCond : PCond
	{
		private PCond _cond_;

		public AOrfolcondCond ()
		{
		}

		public AOrfolcondCond (
			PCond _cond_
		)
		{
			SetCond (_cond_);
		}

		public override object Clone()
		{
			return new AOrfolcondCond (
				(PCond)CloneNode (_cond_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAOrfolcondCond(this);
		}

		public PCond GetCond ()
		{
			return _cond_;
		}

		public void SetCond (PCond node)
		{
			_cond_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_cond_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_cond_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _cond_ == child )
			{
				_cond_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _cond_ == oldChild )
			{
				SetCond ((PCond) newChild);
				return;
			}
		}

	}
}
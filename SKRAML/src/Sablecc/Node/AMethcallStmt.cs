using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AMethcallStmt : PStmt
	{
		private PMcall _mcall_;

		public AMethcallStmt ()
		{
		}

		public AMethcallStmt (
			PMcall _mcall_
		)
		{
			SetMcall (_mcall_);
		}

		public override object Clone()
		{
			return new AMethcallStmt (
				(PMcall)CloneNode (_mcall_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAMethcallStmt(this);
		}

		public PMcall GetMcall ()
		{
			return _mcall_;
		}

		public void SetMcall (PMcall node)
		{
			_mcall_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_mcall_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_mcall_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _mcall_ == child )
			{
				_mcall_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _mcall_ == oldChild )
			{
				SetMcall ((PMcall) newChild);
				return;
			}
		}

	}
}
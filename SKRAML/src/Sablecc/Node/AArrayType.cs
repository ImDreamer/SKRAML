using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AArrayType : PType
	{
		private PType _type_;

		public AArrayType ()
		{
		}

		public AArrayType (
			PType _type_
		)
		{
			SetType (_type_);
		}

		public override object Clone()
		{
			return new AArrayType (
				(PType)CloneNode (_type_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAArrayType(this);
		}

		public new PType GetType ()
		{
			return _type_;
		}

		public void SetType (PType node)
		{
			_type_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_type_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_type_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _type_ == child )
			{
				_type_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _type_ == oldChild )
			{
				SetType ((PType) newChild);
				return;
			}
		}

	}
}
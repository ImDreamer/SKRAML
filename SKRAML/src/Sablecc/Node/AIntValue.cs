using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AIntValue : PValue
	{
		private TIntvalue _intvalue_;

		public AIntValue ()
		{
		}

		public AIntValue (
			TIntvalue _intvalue_
		)
		{
			SetIntvalue (_intvalue_);
		}

		public override object Clone()
		{
			return new AIntValue (
				(TIntvalue)CloneNode (_intvalue_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAIntValue(this);
		}

		public TIntvalue GetIntvalue ()
		{
			return _intvalue_;
		}

		public void SetIntvalue (TIntvalue node)
		{
			_intvalue_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_intvalue_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_intvalue_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _intvalue_ == child )
			{
				_intvalue_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _intvalue_ == oldChild )
			{
				SetIntvalue ((TIntvalue) newChild);
				return;
			}
		}

	}
}
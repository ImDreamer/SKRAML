using System;
using System.Collections;

namespace SKRAML.Sablecc.Node {
	public class TypedList : IList {
		private ArrayList list;
		private Cast cast;

		public TypedList() {
			list = new ArrayList();
			cast = NoCast.Instance;
		}

		public TypedList(ICollection c) {
			list = new ArrayList();
			cast = NoCast.Instance;
			AddAll(c);
		}

		internal TypedList(Cast cast) {
			list = new ArrayList();
			this.cast = cast;
		}

		internal TypedList(ICollection c, Cast cast) {
			list = new ArrayList();
			this.cast = cast;
			AddAll(c);
		}

		internal Cast GetCast() {
			return cast;
		}

		public int Add(object o) {
			return list.Add(cast.Cast(o));
		}

		public void AddAll(ICollection c) {
			foreach (Node n in c) {
				list.Add(cast.Cast(n));
			}
		}

		public int Count {
			get { return list.Count; }
		}

		public bool IsSynchronized {
			get { return list.IsSynchronized; }
		}

		public object SyncRoot {
			get { return list.SyncRoot; }
		}

		public void CopyTo(Array array, int index) {
			list.CopyTo(array, index);
		}

		public bool IsFixedSize {
			get { return list.IsFixedSize; }
		}

		public bool IsReadOnly {
			get { return list.IsReadOnly; }
		}

		public object this[int index] {
			get { return list[index]; }
			set { list[index] = cast.Cast(value); }
		}

		public IEnumerator GetEnumerator() {
			return list.GetEnumerator();
		}

		public void Clear() {
			foreach (Node n in list)
				cast.UnCast(n);
			list.Clear();
		}

		public bool Contains(object value) {
			return list.Contains(value);
		}

		public int IndexOf(object value) {
			return list.IndexOf(value);
		}

		public void Insert(int index, object value) {
			list.Insert(index, cast.Cast(value));
		}

		public void Remove(object value) {
			int n = list.IndexOf(value);
			if (n != -1)
				RemoveAt(n); // this will UnCast
		}

		public void RemoveAt(int index) {
			cast.UnCast(list[index]);
			list.RemoveAt(index);
		}
	}
}
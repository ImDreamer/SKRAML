using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AContinueCondistmts : PCondistmts
	{


		public AContinueCondistmts (
		)
		{
		}

		public override object Clone()
		{
			return new AContinueCondistmts (
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAContinueCondistmts(this);
		}


		public override string ToString()
		{
			return ""
				;
		}

		internal override void RemoveChild(Node child)
		{
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
		}

	}
}
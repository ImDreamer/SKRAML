using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class ACloseScope : PScope
	{


		public ACloseScope (
		)
		{
		}

		public override object Clone()
		{
			return new ACloseScope (
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseACloseScope(this);
		}


		public override string ToString()
		{
			return ""
				;
		}

		internal override void RemoveChild(Node child)
		{
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
		}

	}
}
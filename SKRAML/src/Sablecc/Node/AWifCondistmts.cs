using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AWifCondistmts : PCondistmts
	{
		private PCondiblck _if_;
		private PCondistmts _condistmts_;

		public AWifCondistmts ()
		{
		}

		public AWifCondistmts (
			PCondiblck _if_,
			PCondistmts _condistmts_
		)
		{
			SetIf (_if_);
			SetCondistmts (_condistmts_);
		}

		public override object Clone()
		{
			return new AWifCondistmts (
				(PCondiblck)CloneNode (_if_),
				(PCondistmts)CloneNode (_condistmts_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAWifCondistmts(this);
		}

		public PCondiblck GetIf ()
		{
			return _if_;
		}

		public void SetIf (PCondiblck node)
		{
			_if_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_if_ = node;
		}
		public PCondistmts GetCondistmts ()
		{
			return _condistmts_;
		}

		public void SetCondistmts (PCondistmts node)
		{
			_condistmts_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_condistmts_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_if_)
			       + ToString (_condistmts_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _if_ == child )
			{
				_if_ = null;
				return;
			}
			if ( _condistmts_ == child )
			{
				_condistmts_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _if_ == oldChild )
			{
				SetIf ((PCondiblck) newChild);
				return;
			}
			if ( _condistmts_ == oldChild )
			{
				SetCondistmts ((PCondistmts) newChild);
				return;
			}
		}

	}
}
using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class ABlock : PBlock
	{
		private PBlock _left_;
		private PBlock _right_;

		public ABlock ()
		{
		}

		public ABlock (
			PBlock _left_,
			PBlock _right_
		)
		{
			SetLeft (_left_);
			SetRight (_right_);
		}

		public override object Clone()
		{
			return new ABlock (
				(PBlock)CloneNode (_left_),
				(PBlock)CloneNode (_right_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseABlock(this);
		}

		public PBlock GetLeft ()
		{
			return _left_;
		}

		public void SetLeft (PBlock node)
		{
			_left_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_left_ = node;
		}
		public PBlock GetRight ()
		{
			return _right_;
		}

		public void SetRight (PBlock node)
		{
			_right_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_right_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_left_)
			       + ToString (_right_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _left_ == child )
			{
				_left_ = null;
				return;
			}
			if ( _right_ == child )
			{
				_right_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _left_ == oldChild )
			{
				SetLeft ((PBlock) newChild);
				return;
			}
			if ( _right_ == oldChild )
			{
				SetRight ((PBlock) newChild);
				return;
			}
		}

	}
}
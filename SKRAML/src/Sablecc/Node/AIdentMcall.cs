using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AIdentMcall : PMcall
	{
		private TIdentifier _identifier_;
		private PMcall _mcall_;

		public AIdentMcall ()
		{
		}

		public AIdentMcall (
			TIdentifier _identifier_,
			PMcall _mcall_
		)
		{
			SetIdentifier (_identifier_);
			SetMcall (_mcall_);
		}

		public override object Clone()
		{
			return new AIdentMcall (
				(TIdentifier)CloneNode (_identifier_),
				(PMcall)CloneNode (_mcall_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAIdentMcall(this);
		}

		public TIdentifier GetIdentifier ()
		{
			return _identifier_;
		}

		public void SetIdentifier (TIdentifier node)
		{
			_identifier_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_identifier_ = node;
		}
		public PMcall GetMcall ()
		{
			return _mcall_;
		}

		public void SetMcall (PMcall node)
		{
			_mcall_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_mcall_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_identifier_)
			       + ToString (_mcall_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _identifier_ == child )
			{
				_identifier_ = null;
				return;
			}
			if ( _mcall_ == child )
			{
				_mcall_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _identifier_ == oldChild )
			{
				SetIdentifier ((TIdentifier) newChild);
				return;
			}
			if ( _mcall_ == oldChild )
			{
				SetMcall ((PMcall) newChild);
				return;
			}
		}

	}
}
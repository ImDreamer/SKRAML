using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AFloatType : PType
	{
		private TFloatkeyword _floatkeyword_;

		public AFloatType ()
		{
		}

		public AFloatType (
			TFloatkeyword _floatkeyword_
		)
		{
			SetFloatkeyword (_floatkeyword_);
		}

		public override object Clone()
		{
			return new AFloatType (
				(TFloatkeyword)CloneNode (_floatkeyword_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAFloatType(this);
		}

		public TFloatkeyword GetFloatkeyword ()
		{
			return _floatkeyword_;
		}

		public void SetFloatkeyword (TFloatkeyword node)
		{
			_floatkeyword_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_floatkeyword_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_floatkeyword_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _floatkeyword_ == child )
			{
				_floatkeyword_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _floatkeyword_ == oldChild )
			{
				SetFloatkeyword ((TFloatkeyword) newChild);
				return;
			}
		}

	}
}
using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class ACharValue : PValue
	{
		private TCharvalue _charvalue_;

		public ACharValue ()
		{
		}

		public ACharValue (
			TCharvalue _charvalue_
		)
		{
			SetCharvalue (_charvalue_);
		}

		public override object Clone()
		{
			return new ACharValue (
				(TCharvalue)CloneNode (_charvalue_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseACharValue(this);
		}

		public TCharvalue GetCharvalue ()
		{
			return _charvalue_;
		}

		public void SetCharvalue (TCharvalue node)
		{
			_charvalue_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_charvalue_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_charvalue_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _charvalue_ == child )
			{
				_charvalue_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _charvalue_ == oldChild )
			{
				SetCharvalue ((TCharvalue) newChild);
				return;
			}
		}

	}
}
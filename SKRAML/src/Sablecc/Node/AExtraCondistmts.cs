using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AExtraCondistmts : PCondistmts
	{
		private PCondistmts _condistmts_;

		public AExtraCondistmts ()
		{
		}

		public AExtraCondistmts (
			PCondistmts _condistmts_
		)
		{
			SetCondistmts (_condistmts_);
		}

		public override object Clone()
		{
			return new AExtraCondistmts (
				(PCondistmts)CloneNode (_condistmts_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAExtraCondistmts(this);
		}

		public PCondistmts GetCondistmts ()
		{
			return _condistmts_;
		}

		public void SetCondistmts (PCondistmts node)
		{
			_condistmts_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_condistmts_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_condistmts_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _condistmts_ == child )
			{
				_condistmts_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _condistmts_ == oldChild )
			{
				SetCondistmts ((PCondistmts) newChild);
				return;
			}
		}

	}
}
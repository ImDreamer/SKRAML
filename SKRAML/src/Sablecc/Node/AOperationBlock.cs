using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AOperationBlock : PBlock
	{
		private PBlock _block_;
		private POperation _operation_;

		public AOperationBlock ()
		{
		}

		public AOperationBlock (
			PBlock _block_,
			POperation _operation_
		)
		{
			SetBlock (_block_);
			SetOperation (_operation_);
		}

		public override object Clone()
		{
			return new AOperationBlock (
				(PBlock)CloneNode (_block_),
				(POperation)CloneNode (_operation_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAOperationBlock(this);
		}

		public PBlock GetBlock ()
		{
			return _block_;
		}

		public void SetBlock (PBlock node)
		{
			_block_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_block_ = node;
		}
		public POperation GetOperation ()
		{
			return _operation_;
		}

		public void SetOperation (POperation node)
		{
			_operation_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_operation_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_block_)
			       + ToString (_operation_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _block_ == child )
			{
				_block_ = null;
				return;
			}
			if ( _operation_ == child )
			{
				_operation_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _block_ == oldChild )
			{
				SetBlock ((PBlock) newChild);
				return;
			}
			if ( _operation_ == oldChild )
			{
				SetOperation ((POperation) newChild);
				return;
			}
		}

	}
}
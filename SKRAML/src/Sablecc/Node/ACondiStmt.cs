using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class ACondiStmt : PStmt
	{
		private PCondiblck _condiblck_;

		public ACondiStmt ()
		{
		}

		public ACondiStmt (
			PCondiblck _condiblck_
		)
		{
			SetCondiblck (_condiblck_);
		}

		public override object Clone()
		{
			return new ACondiStmt (
				(PCondiblck)CloneNode (_condiblck_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseACondiStmt(this);
		}

		public PCondiblck GetCondiblck ()
		{
			return _condiblck_;
		}

		public void SetCondiblck (PCondiblck node)
		{
			_condiblck_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_condiblck_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_condiblck_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _condiblck_ == child )
			{
				_condiblck_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _condiblck_ == oldChild )
			{
				SetCondiblck ((PCondiblck) newChild);
				return;
			}
		}

	}
}
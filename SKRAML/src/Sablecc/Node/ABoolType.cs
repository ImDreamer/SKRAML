using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class ABoolType : PType
	{
		private TBoolkeyword _boolkeyword_;

		public ABoolType ()
		{
		}

		public ABoolType (
			TBoolkeyword _boolkeyword_
		)
		{
			SetBoolkeyword (_boolkeyword_);
		}

		public override object Clone()
		{
			return new ABoolType (
				(TBoolkeyword)CloneNode (_boolkeyword_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseABoolType(this);
		}

		public TBoolkeyword GetBoolkeyword ()
		{
			return _boolkeyword_;
		}

		public void SetBoolkeyword (TBoolkeyword node)
		{
			_boolkeyword_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_boolkeyword_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_boolkeyword_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _boolkeyword_ == child )
			{
				_boolkeyword_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _boolkeyword_ == oldChild )
			{
				SetBoolkeyword ((TBoolkeyword) newChild);
				return;
			}
		}

	}
}
using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AFloatValue : PValue
	{
		private TFloatvalue _floatvalue_;

		public AFloatValue ()
		{
		}

		public AFloatValue (
			TFloatvalue _floatvalue_
		)
		{
			SetFloatvalue (_floatvalue_);
		}

		public override object Clone()
		{
			return new AFloatValue (
				(TFloatvalue)CloneNode (_floatvalue_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAFloatValue(this);
		}

		public TFloatvalue GetFloatvalue ()
		{
			return _floatvalue_;
		}

		public void SetFloatvalue (TFloatvalue node)
		{
			_floatvalue_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_floatvalue_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_floatvalue_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _floatvalue_ == child )
			{
				_floatvalue_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _floatvalue_ == oldChild )
			{
				SetFloatvalue ((TFloatvalue) newChild);
				return;
			}
		}

	}
}
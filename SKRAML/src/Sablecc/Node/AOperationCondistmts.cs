using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AOperationCondistmts : PCondistmts
	{
		private POperation _operation_;
		private PCondistmts _condistmts_;

		public AOperationCondistmts ()
		{
		}

		public AOperationCondistmts (
			POperation _operation_,
			PCondistmts _condistmts_
		)
		{
			SetOperation (_operation_);
			SetCondistmts (_condistmts_);
		}

		public override object Clone()
		{
			return new AOperationCondistmts (
				(POperation)CloneNode (_operation_),
				(PCondistmts)CloneNode (_condistmts_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAOperationCondistmts(this);
		}

		public POperation GetOperation ()
		{
			return _operation_;
		}

		public void SetOperation (POperation node)
		{
			_operation_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_operation_ = node;
		}
		public PCondistmts GetCondistmts ()
		{
			return _condistmts_;
		}

		public void SetCondistmts (PCondistmts node)
		{
			_condistmts_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_condistmts_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_operation_)
			       + ToString (_condistmts_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _operation_ == child )
			{
				_operation_ = null;
				return;
			}
			if ( _condistmts_ == child )
			{
				_condistmts_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _operation_ == oldChild )
			{
				SetOperation ((POperation) newChild);
				return;
			}
			if ( _condistmts_ == oldChild )
			{
				SetCondistmts ((PCondistmts) newChild);
				return;
			}
		}

	}
}
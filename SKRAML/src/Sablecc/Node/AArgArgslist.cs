using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Node {
	public sealed class AArgArgslist : PArgslist
	{
		private PType _type_;
		private TIdentifier _identifier_;

		public AArgArgslist ()
		{
		}

		public AArgArgslist (
			PType _type_,
			TIdentifier _identifier_
		)
		{
			SetType (_type_);
			SetIdentifier (_identifier_);
		}

		public override object Clone()
		{
			return new AArgArgslist (
				(PType)CloneNode (_type_),
				(TIdentifier)CloneNode (_identifier_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAArgArgslist(this);
		}

		public new PType GetType ()
		{
			return _type_;
		}

		public void SetType (PType node)
		{
			_type_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_type_ = node;
		}
		public TIdentifier GetIdentifier ()
		{
			return _identifier_;
		}

		public void SetIdentifier (TIdentifier node)
		{
			_identifier_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_identifier_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_type_)
			       + ToString (_identifier_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _type_ == child )
			{
				_type_ = null;
				return;
			}
			if ( _identifier_ == child )
			{
				_identifier_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _type_ == oldChild )
			{
				SetType ((PType) newChild);
				return;
			}
			if ( _identifier_ == oldChild )
			{
				SetIdentifier ((TIdentifier) newChild);
				return;
			}
		}

	}
}
using SKRAML.Sablecc.Analysis;

namespace SKRAML.Sablecc.Node {
	public sealed class AClassesStart : PStart
	{
		private PClasses _classes_;
		private PStart _start_;

		public AClassesStart ()
		{
		}

		public AClassesStart (
			PClasses _classes_,
			PStart _start_
		)
		{
			SetClasses (_classes_);
			SetStart (_start_);
		}

		public override object Clone()
		{
			return new AClassesStart (
				(PClasses)CloneNode (_classes_),
				(PStart)CloneNode (_start_)
			);
		}

		public override void Apply(Switch sw)
		{
			((IAnalysis) sw).CaseAClassesStart(this);
		}

		public PClasses GetClasses ()
		{
			return _classes_;
		}

		public void SetClasses (PClasses node)
		{
			_classes_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_classes_ = node;
		}
		public PStart GetStart ()
		{
			return _start_;
		}

		public void SetStart (PStart node)
		{
			_start_?.Parent(null);

			if(node != null)
			{
				if(node.Parent() != null)
				{
					node.Parent().RemoveChild(node);
				}

				node.Parent(this);
			}

			_start_ = node;
		}

		public override string ToString()
		{
			return ""
			       + ToString (_classes_)
			       + ToString (_start_)
				;
		}

		internal override void RemoveChild(Node child)
		{
			if ( _classes_ == child )
			{
				_classes_ = null;
				return;
			}
			if ( _start_ == child )
			{
				_start_ = null;
				return;
			}
		}

		internal override void ReplaceChild(Node oldChild, Node newChild)
		{
			if ( _classes_ == oldChild )
			{
				SetClasses ((PClasses) newChild);
				return;
			}
			if ( _start_ == oldChild )
			{
				SetStart ((PStart) newChild);
				return;
			}
		}

	}
}
using System;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Parser {
	public class ParserException : ApplicationException {
		Token token;

		public ParserException(Token token, string message) : base(message) {
			this.token = token;
		}

		public Token Token => token;
	}
}
using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Sablecc.Parser {
	internal class TokenIndex : AnalysisAdapter {
		internal int index;

		public override void CaseTBoolkeyword(TBoolkeyword node) {
			index = 0;
		}

		public override void CaseTCharkeyword(TCharkeyword node) {
			index = 1;
		}

		public override void CaseTFloatkeyword(TFloatkeyword node) {
			index = 2;
		}

		public override void CaseTIntkeyword(TIntkeyword node) {
			index = 3;
		}

		public override void CaseTStringkeyword(TStringkeyword node) {
			index = 4;
		}

		public override void CaseTVoidkeyword(TVoidkeyword node) {
			index = 5;
		}

		public override void CaseTBoolvalue(TBoolvalue node) {
			index = 6;
		}

		public override void CaseTIntvalue(TIntvalue node) {
			index = 7;
		}

		public override void CaseTCharvalue(TCharvalue node) {
			index = 8;
		}

		public override void CaseTFloatvalue(TFloatvalue node) {
			index = 9;
		}

		public override void CaseTStringvalue(TStringvalue node) {
			index = 10;
		}

		public override void CaseTLParen(TLParen node) {
			index = 11;
		}

		public override void CaseTRParen(TRParen node) {
			index = 12;
		}

		public override void CaseTLCurly(TLCurly node) {
			index = 13;
		}

		public override void CaseTRCurly(TRCurly node) {
			index = 14;
		}

		public override void CaseTLSquare(TLSquare node) {
			index = 15;
		}

		public override void CaseTRSquare(TRSquare node) {
			index = 16;
		}

		public override void CaseTDot(TDot node) {
			index = 17;
		}

		public override void CaseTBackslash(TBackslash node) {
			index = 18;
		}

		public override void CaseTClassl(TClassl node) {
			index = 19;
		}

		public override void CaseTReturn(TReturn node) {
			index = 20;
		}

		public override void CaseTNewl(TNewl node) {
			index = 21;
		}

		public override void CaseTSemicol(TSemicol node) {
			index = 22;
		}

		public override void CaseTEquals(TEquals node) {
			index = 23;
		}

		public override void CaseTOp(TOp node) {
			index = 24;
		}

		public override void CaseTUpperops(TUpperops node) {
			index = 25;
		}

		public override void CaseTIfl(TIfl node) {
			index = 26;
		}

		public override void CaseTElsifl(TElsifl node) {
			index = 27;
		}

		public override void CaseTElsel(TElsel node) {
			index = 28;
		}

		public override void CaseTWhilel(TWhilel node) {
			index = 29;
		}

		public override void CaseTBreak(TBreak node) {
			index = 30;
		}

		public override void CaseTContinue(TContinue node) {
			index = 31;
		}

		public override void CaseTCompop(TCompop node) {
			index = 32;
		}

		public override void CaseTAmpsand(TAmpsand node) {
			index = 33;
		}

		public override void CaseTPipe(TPipe node) {
			index = 34;
		}

		public override void CaseTComma(TComma node) {
			index = 35;
		}

		public override void CaseTIdentifier(TIdentifier node) {
			index = 36;
		}

		public override void CaseEOF(EOF node) {
			index = 37;
		}
	}
}
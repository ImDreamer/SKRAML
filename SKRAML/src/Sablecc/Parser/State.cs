using System.Collections;

namespace SKRAML.Sablecc.Parser {
	internal class State {
		internal int state;
		internal ArrayList nodes;

		internal State(int state, ArrayList nodes) {
			this.state = state;
			this.nodes = nodes;
		}
	}
}
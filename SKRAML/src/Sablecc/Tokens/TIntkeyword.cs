using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Node;

namespace SKRAML.Sablecc.Tokens {
	public sealed class TIntkeyword : Token {
		public TIntkeyword(string text) {
			Text = text;
		}

		public TIntkeyword(string text, int line, int pos) {
			Text = text;
			Line = line;
			Pos = pos;
		}

		public override object Clone() {
			return new TIntkeyword(Text, Line, Pos);
		}

		public override void Apply(Switch sw) {
			((IAnalysis) sw).CaseTIntkeyword(this);
		}
	}
}
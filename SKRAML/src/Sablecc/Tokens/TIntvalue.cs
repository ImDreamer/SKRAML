using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Node;

namespace SKRAML.Sablecc.Tokens {
	public sealed class TIntvalue : Token {
		public TIntvalue(string text) {
			Text = text;
		}

		public TIntvalue(string text, int line, int pos) {
			Text = text;
			Line = line;
			Pos = pos;
		}

		public override object Clone() {
			return new TIntvalue(Text, Line, Pos);
		}

		public override void Apply(Switch sw) {
			((IAnalysis) sw).CaseTIntvalue(this);
		}
	}
}
using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Node;

namespace SKRAML.Sablecc.Tokens {
	public sealed class TComment : Token {
		public TComment(string text) {
			Text = text;
		}

		public TComment(string text, int line, int pos) {
			Text = text;
			Line = line;
			Pos = pos;
		}

		public override object Clone() {
			return new TComment(Text, Line, Pos);
		}

		public override void Apply(Switch sw) {
			((IAnalysis) sw).CaseTComment(this);
		}
	}
}
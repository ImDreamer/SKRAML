namespace SKRAML.Sablecc.Tokens {
	public abstract class Token : Node.Node {
		private string text;
		private int line;
		private int pos;

		public virtual string Text {
			get { return text; }
			set { text = value; }
		}

		public int Line {
			get { return line; }
			set { line = value; }
		}

		public int Pos {
			get { return pos; }
			set { pos = value; }
		}

		public override string ToString() {
			return text + " ";
		}

		internal override void RemoveChild(Node.Node child) { }

		internal override void ReplaceChild(Node.Node oldChild, Node.Node newChild) { }
	}
}
using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Node;

namespace SKRAML.Sablecc.Tokens {
	public sealed class TVoidkeyword : Token {
		public TVoidkeyword(string text) {
			Text = text;
		}

		public TVoidkeyword(string text, int line, int pos) {
			Text = text;
			Line = line;
			Pos = pos;
		}

		public override object Clone() {
			return new TVoidkeyword(Text, Line, Pos);
		}

		public override void Apply(Switch sw) {
			((IAnalysis) sw).CaseTVoidkeyword(this);
		}
	}
}
using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Node;

namespace SKRAML.Sablecc.Tokens {
	public sealed class TReturn : Token {
		public TReturn(string text) {
			Text = text;
		}

		public TReturn(string text, int line, int pos) {
			Text = text;
			Line = line;
			Pos = pos;
		}

		public override object Clone() {
			return new TReturn(Text, Line, Pos);
		}

		public override void Apply(Switch sw) {
			((IAnalysis) sw).CaseTReturn(this);
		}
	}
}
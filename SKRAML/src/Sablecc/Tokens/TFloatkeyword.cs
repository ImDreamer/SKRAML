using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Node;

namespace SKRAML.Sablecc.Tokens {
	public sealed class TFloatkeyword : Token {
		public TFloatkeyword(string text) {
			Text = text;
		}

		public TFloatkeyword(string text, int line, int pos) {
			Text = text;
			Line = line;
			Pos = pos;
		}

		public override object Clone() {
			return new TFloatkeyword(Text, Line, Pos);
		}

		public override void Apply(Switch sw) {
			((IAnalysis) sw).CaseTFloatkeyword(this);
		}
	}
}
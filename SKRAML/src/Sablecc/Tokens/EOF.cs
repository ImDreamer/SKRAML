using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Node;

namespace SKRAML.Sablecc.Tokens {
	public sealed class EOF : Token {
		public EOF() {
			Text = "";
		}

		public EOF(int line, int pos) {
			Text = "";
			Line = line;
			Pos = pos;
		}

		public override object Clone() {
			return new EOF(Line, Pos);
		}

		public override void Apply(Switch sw) {
			((IAnalysis) sw).CaseEOF(this);
		}
	}
}
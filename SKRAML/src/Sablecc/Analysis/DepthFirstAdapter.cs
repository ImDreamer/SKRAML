using SKRAML.Sablecc.Node;

namespace SKRAML.Sablecc.Analysis {
	public class DepthFirstAdapter : AnalysisAdapter {
		public virtual void InStart(Start node) {
			DefaultIn(node);
		}

		public virtual void OutStart(Start node) {
			DefaultOut(node);
		}

		public virtual void DefaultIn(Node.Node node) { }

		public virtual void DefaultOut(Node.Node node) { }

		public override void CaseStart(Start node) {
			InStart(node);
			node.GetPStart().Apply(this);
			node.GetEOF().Apply(this);
			OutStart(node);
		}

		public virtual void InAStart(AStart node) {
			DefaultIn(node);
		}

		public virtual void OutAStart(AStart node) {
			DefaultOut(node);
		}

		public override void CaseAStart(AStart node) {
			InAStart(node);
			if (node.GetStart() != null) {
				node.GetStart().Apply(this);
			}

			OutAStart(node);
		}

		public virtual void InAProgramStart(AProgramStart node) {
			DefaultIn(node);
		}

		public virtual void OutAProgramStart(AProgramStart node) {
			DefaultOut(node);
		}

		public override void CaseAProgramStart(AProgramStart node) {
			InAProgramStart(node);
			if (node.GetStart() != null) {
				node.GetStart().Apply(this);
			}

			OutAProgramStart(node);
		}

		public virtual void InADclsStart(ADclsStart node) {
			DefaultIn(node);
		}

		public virtual void OutADclsStart(ADclsStart node) {
			DefaultOut(node);
		}

		public override void CaseADclsStart(ADclsStart node) {
			InADclsStart(node);
			if (node.GetOperation() != null) {
				node.GetOperation().Apply(this);
			}

			if (node.GetStart() != null) {
				node.GetStart().Apply(this);
			}

			OutADclsStart(node);
		}

		public virtual void InAMethodsStart(AMethodsStart node) {
			DefaultIn(node);
		}

		public virtual void OutAMethodsStart(AMethodsStart node) {
			DefaultOut(node);
		}

		public override void CaseAMethodsStart(AMethodsStart node) {
			InAMethodsStart(node);
			if (node.GetMethod() != null) {
				node.GetMethod().Apply(this);
			}

			if (node.GetStart() != null) {
				node.GetStart().Apply(this);
			}

			OutAMethodsStart(node);
		}

		public virtual void InAClassesStart(AClassesStart node) {
			DefaultIn(node);
		}

		public virtual void OutAClassesStart(AClassesStart node) {
			DefaultOut(node);
		}

		public override void CaseAClassesStart(AClassesStart node) {
			InAClassesStart(node);
			if (node.GetClasses() != null) {
				node.GetClasses().Apply(this);
			}

			if (node.GetStart() != null) {
				node.GetStart().Apply(this);
			}

			OutAClassesStart(node);
		}

		public virtual void InADclStart(ADclStart node) {
			DefaultIn(node);
		}

		public virtual void OutADclStart(ADclStart node) {
			DefaultOut(node);
		}

		public override void CaseADclStart(ADclStart node) {
			InADclStart(node);
			if (node.GetDcl() != null) {
				node.GetDcl().Apply(this);
			}

			if (node.GetStart() != null) {
				node.GetStart().Apply(this);
			}

			OutADclStart(node);
		}

		public virtual void InAEofStart(AEofStart node) {
			DefaultIn(node);
		}

		public virtual void OutAEofStart(AEofStart node) {
			DefaultOut(node);
		}

		public override void CaseAEofStart(AEofStart node) {
			InAEofStart(node);
			OutAEofStart(node);
		}

		public virtual void InAClasses(AClasses node) {
			DefaultIn(node);
		}

		public virtual void OutAClasses(AClasses node) {
			DefaultOut(node);
		}

		public override void CaseAClasses(AClasses node) {
			InAClasses(node);
			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			if (node.GetStart() != null) {
				node.GetStart().Apply(this);
			}

			OutAClasses(node);
		}

		public virtual void InAMethod(AMethod node) {
			DefaultIn(node);
		}

		public virtual void OutAMethod(AMethod node) {
			DefaultOut(node);
		}

		public override void CaseAMethod(AMethod node) {
			InAMethod(node);
			if (node.GetType() != null) {
				node.GetType().Apply(this);
			}

			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			if (node.GetArgslist() != null) {
				node.GetArgslist().Apply(this);
			}

			if (node.GetBlock() != null) {
				node.GetBlock().Apply(this);
			}

			OutAMethod(node);
		}

		public virtual void InAArgslist(AArgslist node) {
			DefaultIn(node);
		}

		public virtual void OutAArgslist(AArgslist node) {
			DefaultOut(node);
		}

		public override void CaseAArgslist(AArgslist node) {
			InAArgslist(node);
			if (node.GetLeft() != null) {
				node.GetLeft().Apply(this);
			}

			if (node.GetRight() != null) {
				node.GetRight().Apply(this);
			}

			OutAArgslist(node);
		}

		public virtual void InAExtraArgslist(AExtraArgslist node) {
			DefaultIn(node);
		}

		public virtual void OutAExtraArgslist(AExtraArgslist node) {
			DefaultOut(node);
		}

		public override void CaseAExtraArgslist(AExtraArgslist node) {
			InAExtraArgslist(node);
			if (node.GetArgslist() != null) {
				node.GetArgslist().Apply(this);
			}

			OutAExtraArgslist(node);
		}

		public virtual void InAArgArgslist(AArgArgslist node) {
			DefaultIn(node);
		}

		public virtual void OutAArgArgslist(AArgArgslist node) {
			DefaultOut(node);
		}

		public override void CaseAArgArgslist(AArgArgslist node) {
			InAArgArgslist(node);
			if (node.GetType() != null) {
				node.GetType().Apply(this);
			}

			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			OutAArgArgslist(node);
		}

		public virtual void InAIdentifierType(AIdentifierType node) {
			DefaultIn(node);
		}

		public virtual void OutAIdentifierType(AIdentifierType node) {
			DefaultOut(node);
		}

		public override void CaseAIdentifierType(AIdentifierType node) {
			InAIdentifierType(node);
			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			OutAIdentifierType(node);
		}

		public virtual void InABoolType(ABoolType node) {
			DefaultIn(node);
		}

		public virtual void OutABoolType(ABoolType node) {
			DefaultOut(node);
		}

		public override void CaseABoolType(ABoolType node) {
			InABoolType(node);
			if (node.GetBoolkeyword() != null) {
				node.GetBoolkeyword().Apply(this);
			}

			OutABoolType(node);
		}

		public virtual void InACharType(ACharType node) {
			DefaultIn(node);
		}

		public virtual void OutACharType(ACharType node) {
			DefaultOut(node);
		}

		public override void CaseACharType(ACharType node) {
			InACharType(node);
			if (node.GetCharkeyword() != null) {
				node.GetCharkeyword().Apply(this);
			}

			OutACharType(node);
		}

		public virtual void InAFloatType(AFloatType node) {
			DefaultIn(node);
		}

		public virtual void OutAFloatType(AFloatType node) {
			DefaultOut(node);
		}

		public override void CaseAFloatType(AFloatType node) {
			InAFloatType(node);
			if (node.GetFloatkeyword() != null) {
				node.GetFloatkeyword().Apply(this);
			}

			OutAFloatType(node);
		}

		public virtual void InAIntType(AIntType node) {
			DefaultIn(node);
		}

		public virtual void OutAIntType(AIntType node) {
			DefaultOut(node);
		}

		public override void CaseAIntType(AIntType node) {
			InAIntType(node);
			if (node.GetIntkeyword() != null) {
				node.GetIntkeyword().Apply(this);
			}

			OutAIntType(node);
		}

		public virtual void InAStringType(AStringType node) {
			DefaultIn(node);
		}

		public virtual void OutAStringType(AStringType node) {
			DefaultOut(node);
		}

		public override void CaseAStringType(AStringType node) {
			InAStringType(node);
			if (node.GetStringkeyword() != null) {
				node.GetStringkeyword().Apply(this);
			}

			OutAStringType(node);
		}

		public virtual void InAVoidType(AVoidType node) {
			DefaultIn(node);
		}

		public virtual void OutAVoidType(AVoidType node) {
			DefaultOut(node);
		}

		public override void CaseAVoidType(AVoidType node) {
			InAVoidType(node);
			if (node.GetVoidkeyword() != null) {
				node.GetVoidkeyword().Apply(this);
			}

			OutAVoidType(node);
		}

		public virtual void InAOperationType(AOperationType node) {
			DefaultIn(node);
		}

		public virtual void OutAOperationType(AOperationType node) {
			DefaultOut(node);
		}

		public override void CaseAOperationType(AOperationType node) {
			InAOperationType(node);
			if (node.GetOperation() != null) {
				node.GetOperation().Apply(this);
			}

			OutAOperationType(node);
		}

		public virtual void InAArrayType(AArrayType node) {
			DefaultIn(node);
		}

		public virtual void OutAArrayType(AArrayType node) {
			DefaultOut(node);
		}

		public override void CaseAArrayType(AArrayType node) {
			InAArrayType(node);
			if (node.GetType() != null) {
				node.GetType().Apply(this);
			}

			OutAArrayType(node);
		}

		public virtual void InAIdentifierValue(AIdentifierValue node) {
			DefaultIn(node);
		}

		public virtual void OutAIdentifierValue(AIdentifierValue node) {
			DefaultOut(node);
		}

		public override void CaseAIdentifierValue(AIdentifierValue node) {
			InAIdentifierValue(node);
			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			OutAIdentifierValue(node);
		}

		public virtual void InABoolValue(ABoolValue node) {
			DefaultIn(node);
		}

		public virtual void OutABoolValue(ABoolValue node) {
			DefaultOut(node);
		}

		public override void CaseABoolValue(ABoolValue node) {
			InABoolValue(node);
			if (node.GetBoolvalue() != null) {
				node.GetBoolvalue().Apply(this);
			}

			OutABoolValue(node);
		}

		public virtual void InAIntValue(AIntValue node) {
			DefaultIn(node);
		}

		public virtual void OutAIntValue(AIntValue node) {
			DefaultOut(node);
		}

		public override void CaseAIntValue(AIntValue node) {
			InAIntValue(node);
			if (node.GetIntvalue() != null) {
				node.GetIntvalue().Apply(this);
			}

			OutAIntValue(node);
		}

		public virtual void InACharValue(ACharValue node) {
			DefaultIn(node);
		}

		public virtual void OutACharValue(ACharValue node) {
			DefaultOut(node);
		}

		public override void CaseACharValue(ACharValue node) {
			InACharValue(node);
			if (node.GetCharvalue() != null) {
				node.GetCharvalue().Apply(this);
			}

			OutACharValue(node);
		}

		public virtual void InAFloatValue(AFloatValue node) {
			DefaultIn(node);
		}

		public virtual void OutAFloatValue(AFloatValue node) {
			DefaultOut(node);
		}

		public override void CaseAFloatValue(AFloatValue node) {
			InAFloatValue(node);
			if (node.GetFloatvalue() != null) {
				node.GetFloatvalue().Apply(this);
			}

			OutAFloatValue(node);
		}

		public virtual void InAStringValue(AStringValue node) {
			DefaultIn(node);
		}

		public virtual void OutAStringValue(AStringValue node) {
			DefaultOut(node);
		}

		public override void CaseAStringValue(AStringValue node) {
			InAStringValue(node);
			if (node.GetStringvalue() != null) {
				node.GetStringvalue().Apply(this);
			}

			OutAStringValue(node);
		}

		public virtual void InAArrayvalueValue(AArrayvalueValue node) {
			DefaultIn(node);
		}

		public virtual void OutAArrayvalueValue(AArrayvalueValue node) {
			DefaultOut(node);
		}

		public override void CaseAArrayvalueValue(AArrayvalueValue node) {
			InAArrayvalueValue(node);
			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			if (node.GetValue() != null) {
				node.GetValue().Apply(this);
			}

			OutAArrayvalueValue(node);
		}

		public virtual void InAArrayvalueintValue(AArrayvalueintValue node) {
			DefaultIn(node);
		}

		public virtual void OutAArrayvalueintValue(AArrayvalueintValue node) {
			DefaultOut(node);
		}

		public override void CaseAArrayvalueintValue(AArrayvalueintValue node) {
			InAArrayvalueintValue(node);
			if (node.GetIntvalue() != null) {
				node.GetIntvalue().Apply(this);
			}

			OutAArrayvalueintValue(node);
		}

		public virtual void InAArrayvalueidentValue(AArrayvalueidentValue node) {
			DefaultIn(node);
		}

		public virtual void OutAArrayvalueidentValue(AArrayvalueidentValue node) {
			DefaultOut(node);
		}

		public override void CaseAArrayvalueidentValue(AArrayvalueidentValue node) {
			InAArrayvalueidentValue(node);
			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			OutAArrayvalueidentValue(node);
		}

		public virtual void InABlock(ABlock node) {
			DefaultIn(node);
		}

		public virtual void OutABlock(ABlock node) {
			DefaultOut(node);
		}

		public override void CaseABlock(ABlock node) {
			InABlock(node);
			if (node.GetLeft() != null) {
				node.GetLeft().Apply(this);
			}

			if (node.GetRight() != null) {
				node.GetRight().Apply(this);
			}

			OutABlock(node);
		}

		public virtual void InAReturnBlock(AReturnBlock node) {
			DefaultIn(node);
		}

		public virtual void OutAReturnBlock(AReturnBlock node) {
			DefaultOut(node);
		}

		public override void CaseAReturnBlock(AReturnBlock node) {
			InAReturnBlock(node);
			if (node.GetValue() != null) {
				node.GetValue().Apply(this);
			}

			OutAReturnBlock(node);
		}

		public virtual void InAMethodBlock(AMethodBlock node) {
			DefaultIn(node);
		}

		public virtual void OutAMethodBlock(AMethodBlock node) {
			DefaultOut(node);
		}

		public override void CaseAMethodBlock(AMethodBlock node) {
			InAMethodBlock(node);
			if (node.GetBlock() != null) {
				node.GetBlock().Apply(this);
			}

			if (node.GetMethod() != null) {
				node.GetMethod().Apply(this);
			}

			OutAMethodBlock(node);
		}

		public virtual void InAStmtsBlock(AStmtsBlock node) {
			DefaultIn(node);
		}

		public virtual void OutAStmtsBlock(AStmtsBlock node) {
			DefaultOut(node);
		}

		public override void CaseAStmtsBlock(AStmtsBlock node) {
			InAStmtsBlock(node);
			if (node.GetBlock() != null) {
				node.GetBlock().Apply(this);
			}

			if (node.GetStmt() != null) {
				node.GetStmt().Apply(this);
			}

			OutAStmtsBlock(node);
		}

		public virtual void InAOperationBlock(AOperationBlock node) {
			DefaultIn(node);
		}

		public virtual void OutAOperationBlock(AOperationBlock node) {
			DefaultOut(node);
		}

		public override void CaseAOperationBlock(AOperationBlock node) {
			InAOperationBlock(node);
			if (node.GetBlock() != null) {
				node.GetBlock().Apply(this);
			}

			if (node.GetOperation() != null) {
				node.GetOperation().Apply(this);
			}

			OutAOperationBlock(node);
		}

		public virtual void InADclBlock(ADclBlock node) {
			DefaultIn(node);
		}

		public virtual void OutADclBlock(ADclBlock node) {
			DefaultOut(node);
		}

		public override void CaseADclBlock(ADclBlock node) {
			InADclBlock(node);
			if (node.GetBlock() != null) {
				node.GetBlock().Apply(this);
			}

			if (node.GetDcl() != null) {
				node.GetDcl().Apply(this);
			}

			OutADclBlock(node);
		}

		public virtual void InAMethcallStmt(AMethcallStmt node) {
			DefaultIn(node);
		}

		public virtual void OutAMethcallStmt(AMethcallStmt node) {
			DefaultOut(node);
		}

		public override void CaseAMethcallStmt(AMethcallStmt node) {
			InAMethcallStmt(node);
			if (node.GetMcall() != null) {
				node.GetMcall().Apply(this);
			}

			OutAMethcallStmt(node);
		}

		public virtual void InAIdentcallStmt(AIdentcallStmt node) {
			DefaultIn(node);
		}

		public virtual void OutAIdentcallStmt(AIdentcallStmt node) {
			DefaultOut(node);
		}

		public override void CaseAIdentcallStmt(AIdentcallStmt node) {
			InAIdentcallStmt(node);
			if (node.GetMcall() != null) {
				node.GetMcall().Apply(this);
			}

			OutAIdentcallStmt(node);
		}

		public virtual void InACondiStmt(ACondiStmt node) {
			DefaultIn(node);
		}

		public virtual void OutACondiStmt(ACondiStmt node) {
			DefaultOut(node);
		}

		public override void CaseACondiStmt(ACondiStmt node) {
			InACondiStmt(node);
			if (node.GetCondiblck() != null) {
				node.GetCondiblck().Apply(this);
			}

			OutACondiStmt(node);
		}

		public virtual void InAMcall(AMcall node) {
			DefaultIn(node);
		}

		public virtual void OutAMcall(AMcall node) {
			DefaultOut(node);
		}

		public override void CaseAMcall(AMcall node) {
			InAMcall(node);
			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			if (node.GetAclist() != null) {
				node.GetAclist().Apply(this);
			}

			if (node.GetMcall() != null) {
				node.GetMcall().Apply(this);
			}

			OutAMcall(node);
		}

		public virtual void InAIdentMcall(AIdentMcall node) {
			DefaultIn(node);
		}

		public virtual void OutAIdentMcall(AIdentMcall node) {
			DefaultOut(node);
		}

		public override void CaseAIdentMcall(AIdentMcall node) {
			InAIdentMcall(node);
			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			if (node.GetMcall() != null) {
				node.GetMcall().Apply(this);
			}

			OutAIdentMcall(node);
		}

		public virtual void InAIcallMcall(AIcallMcall node) {
			DefaultIn(node);
		}

		public virtual void OutAIcallMcall(AIcallMcall node) {
			DefaultOut(node);
		}

		public override void CaseAIcallMcall(AIcallMcall node) {
			InAIcallMcall(node);
			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			if (node.GetMcall() != null) {
				node.GetMcall().Apply(this);
			}

			OutAIcallMcall(node);
		}

		public virtual void InAAclist(AAclist node) {
			DefaultIn(node);
		}

		public virtual void OutAAclist(AAclist node) {
			DefaultOut(node);
		}

		public override void CaseAAclist(AAclist node) {
			InAAclist(node);
			if (node.GetLeft() != null) {
				node.GetLeft().Apply(this);
			}

			if (node.GetRight() != null) {
				node.GetRight().Apply(this);
			}

			OutAAclist(node);
		}

		public virtual void InAExtraAclist(AExtraAclist node) {
			DefaultIn(node);
		}

		public virtual void OutAExtraAclist(AExtraAclist node) {
			DefaultOut(node);
		}

		public override void CaseAExtraAclist(AExtraAclist node) {
			InAExtraAclist(node);
			if (node.GetAclist() != null) {
				node.GetAclist().Apply(this);
			}

			OutAExtraAclist(node);
		}

		public virtual void InAArgAclist(AArgAclist node) {
			DefaultIn(node);
		}

		public virtual void OutAArgAclist(AArgAclist node) {
			DefaultOut(node);
		}

		public override void CaseAArgAclist(AArgAclist node) {
			InAArgAclist(node);
			if (node.GetValue() != null) {
				node.GetValue().Apply(this);
			}

			OutAArgAclist(node);
		}

		public virtual void InAArgmcallAclist(AArgmcallAclist node) {
			DefaultIn(node);
		}

		public virtual void OutAArgmcallAclist(AArgmcallAclist node) {
			DefaultOut(node);
		}

		public override void CaseAArgmcallAclist(AArgmcallAclist node) {
			InAArgmcallAclist(node);
			if (node.GetMcall() != null) {
				node.GetMcall().Apply(this);
			}

			OutAArgmcallAclist(node);
		}

		public virtual void InADcl(ADcl node) {
			DefaultIn(node);
		}

		public virtual void OutADcl(ADcl node) {
			DefaultOut(node);
		}

		public override void CaseADcl(ADcl node) {
			InADcl(node);
			if (node.GetType() != null) {
				node.GetType().Apply(this);
			}

			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			OutADcl(node);
		}

		public virtual void InAOperation(AOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAOperation(AOperation node) {
			DefaultOut(node);
		}

		public override void CaseAOperation(AOperation node) {
			InAOperation(node);
			if (node.GetType() != null) {
				node.GetType().Apply(this);
			}

			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			if (node.GetOperation() != null) {
				node.GetOperation().Apply(this);
			}

			OutAOperation(node);
		}

		public virtual void InAMassOperation(AMassOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAMassOperation(AMassOperation node) {
			DefaultOut(node);
		}

		public override void CaseAMassOperation(AMassOperation node) {
			InAMassOperation(node);
			if (node.GetMcall() != null) {
				node.GetMcall().Apply(this);
			}

			if (node.GetOperation() != null) {
				node.GetOperation().Apply(this);
			}

			OutAMassOperation(node);
		}

		public virtual void InAValsingOperation(AValsingOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAValsingOperation(AValsingOperation node) {
			DefaultOut(node);
		}

		public override void CaseAValsingOperation(AValsingOperation node) {
			InAValsingOperation(node);
			if (node.GetLeft() != null) {
				node.GetLeft().Apply(this);
			}

			if (node.GetOp() != null) {
				node.GetOp().Apply(this);
			}

			if (node.GetRight() != null) {
				node.GetRight().Apply(this);
			}

			OutAValsingOperation(node);
		}

		public virtual void InAValrecursiveOperation(AValrecursiveOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAValrecursiveOperation(AValrecursiveOperation node) {
			DefaultOut(node);
		}

		public override void CaseAValrecursiveOperation(AValrecursiveOperation node) {
			InAValrecursiveOperation(node);
			if (node.GetLeft() != null) {
				node.GetLeft().Apply(this);
			}

			if (node.GetOp() != null) {
				node.GetOp().Apply(this);
			}

			if (node.GetRight() != null) {
				node.GetRight().Apply(this);
			}

			OutAValrecursiveOperation(node);
		}

		public virtual void InAValupsingOperation(AValupsingOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAValupsingOperation(AValupsingOperation node) {
			DefaultOut(node);
		}

		public override void CaseAValupsingOperation(AValupsingOperation node) {
			InAValupsingOperation(node);
			if (node.GetOperation() != null) {
				node.GetOperation().Apply(this);
			}

			OutAValupsingOperation(node);
		}

		public virtual void InAValurecOperation(AValurecOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAValurecOperation(AValurecOperation node) {
			DefaultOut(node);
		}

		public override void CaseAValurecOperation(AValurecOperation node) {
			InAValurecOperation(node);
			if (node.GetOperation() != null) {
				node.GetOperation().Apply(this);
			}

			if (node.GetUpperops() != null) {
				node.GetUpperops().Apply(this);
			}

			if (node.GetValue() != null) {
				node.GetValue().Apply(this);
			}

			OutAValurecOperation(node);
		}

		public virtual void InAArrayOperation(AArrayOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAArrayOperation(AArrayOperation node) {
			DefaultOut(node);
		}

		public override void CaseAArrayOperation(AArrayOperation node) {
			InAArrayOperation(node);
			if (node.GetType() != null) {
				node.GetType().Apply(this);
			}

			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			if (node.GetValue() != null) {
				node.GetValue().Apply(this);
			}

			OutAArrayOperation(node);
		}

		public virtual void InAArrayobjOperation(AArrayobjOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAArrayobjOperation(AArrayobjOperation node) {
			DefaultOut(node);
		}

		public override void CaseAArrayobjOperation(AArrayobjOperation node) {
			InAArrayobjOperation(node);
			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			if (node.GetValue() != null) {
				node.GetValue().Apply(this);
			}

			OutAArrayobjOperation(node);
		}

		public virtual void InAArrayassOperation(AArrayassOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAArrayassOperation(AArrayassOperation node) {
			DefaultOut(node);
		}

		public override void CaseAArrayassOperation(AArrayassOperation node) {
			InAArrayassOperation(node);
			if (node.GetIdentifier() != null) {
				node.GetIdentifier().Apply(this);
			}

			if (node.GetValue() != null) {
				node.GetValue().Apply(this);
			}

			if (node.GetOperation() != null) {
				node.GetOperation().Apply(this);
			}

			OutAArrayassOperation(node);
		}

		public virtual void InASinglevalOperation(ASinglevalOperation node) {
			DefaultIn(node);
		}

		public virtual void OutASinglevalOperation(ASinglevalOperation node) {
			DefaultOut(node);
		}

		public override void CaseASinglevalOperation(ASinglevalOperation node) {
			InASinglevalOperation(node);
			if (node.GetValue() != null) {
				node.GetValue().Apply(this);
			}

			OutASinglevalOperation(node);
		}

		public virtual void InAMethodcallOperation(AMethodcallOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAMethodcallOperation(AMethodcallOperation node) {
			DefaultOut(node);
		}

		public override void CaseAMethodcallOperation(AMethodcallOperation node) {
			InAMethodcallOperation(node);
			if (node.GetMcall() != null) {
				node.GetMcall().Apply(this);
			}

			OutAMethodcallOperation(node);
		}

		public virtual void InAValhrecOperation(AValhrecOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAValhrecOperation(AValhrecOperation node) {
			DefaultOut(node);
		}

		public override void CaseAValhrecOperation(AValhrecOperation node) {
			InAValhrecOperation(node);
			if (node.GetOperation() != null) {
				node.GetOperation().Apply(this);
			}

			OutAValhrecOperation(node);
		}

		public virtual void InAObjectOperation(AObjectOperation node) {
			DefaultIn(node);
		}

		public virtual void OutAObjectOperation(AObjectOperation node) {
			DefaultOut(node);
		}

		public override void CaseAObjectOperation(AObjectOperation node) {
			InAObjectOperation(node);
			if (node.GetType() != null) {
				node.GetType().Apply(this);
			}

			if (node.GetName() != null) {
				node.GetName().Apply(this);
			}

			OutAObjectOperation(node);
		}

		public virtual void InAIfCondiblck(AIfCondiblck node) {
			DefaultIn(node);
		}

		public virtual void OutAIfCondiblck(AIfCondiblck node) {
			DefaultOut(node);
		}

		public override void CaseAIfCondiblck(AIfCondiblck node) {
			InAIfCondiblck(node);
			if (node.GetOpen() != null) {
				node.GetOpen().Apply(this);
			}

			if (node.GetCond() != null) {
				node.GetCond().Apply(this);
			}

			if (node.GetBlock() != null) {
				node.GetBlock().Apply(this);
			}

			if (node.GetLoopmod() != null) {
				node.GetLoopmod().Apply(this);
			}

			if (node.GetClose() != null) {
				node.GetClose().Apply(this);
			}

			if (node.GetElsif() != null) {
				node.GetElsif().Apply(this);
			}

			if (node.GetElse() != null) {
				node.GetElse().Apply(this);
			}

			OutAIfCondiblck(node);
		}

		public virtual void InAElsifCondiblck(AElsifCondiblck node) {
			DefaultIn(node);
		}

		public virtual void OutAElsifCondiblck(AElsifCondiblck node) {
			DefaultOut(node);
		}

		public override void CaseAElsifCondiblck(AElsifCondiblck node) {
			InAElsifCondiblck(node);
			if (node.GetOpen() != null) {
				node.GetOpen().Apply(this);
			}

			if (node.GetCond() != null) {
				node.GetCond().Apply(this);
			}

			if (node.GetBlock() != null) {
				node.GetBlock().Apply(this);
			}

			if (node.GetLoopmod() != null) {
				node.GetLoopmod().Apply(this);
			}

			if (node.GetClose() != null) {
				node.GetClose().Apply(this);
			}

			if (node.GetElsif() != null) {
				node.GetElsif().Apply(this);
			}

			OutAElsifCondiblck(node);
		}

		public virtual void InAElseCondiblck(AElseCondiblck node) {
			DefaultIn(node);
		}

		public virtual void OutAElseCondiblck(AElseCondiblck node) {
			DefaultOut(node);
		}

		public override void CaseAElseCondiblck(AElseCondiblck node) {
			InAElseCondiblck(node);
			if (node.GetOpen() != null) {
				node.GetOpen().Apply(this);
			}

			if (node.GetBlock() != null) {
				node.GetBlock().Apply(this);
			}

			if (node.GetLoopmod() != null) {
				node.GetLoopmod().Apply(this);
			}

			if (node.GetClose() != null) {
				node.GetClose().Apply(this);
			}

			OutAElseCondiblck(node);
		}

		public virtual void InAWhileCondiblck(AWhileCondiblck node) {
			DefaultIn(node);
		}

		public virtual void OutAWhileCondiblck(AWhileCondiblck node) {
			DefaultOut(node);
		}

		public override void CaseAWhileCondiblck(AWhileCondiblck node) {
			InAWhileCondiblck(node);
			if (node.GetOpen() != null) {
				node.GetOpen().Apply(this);
			}

			if (node.GetCond() != null) {
				node.GetCond().Apply(this);
			}

			if (node.GetCondistmts() != null) {
				node.GetCondistmts().Apply(this);
			}

			if (node.GetClose() != null) {
				node.GetClose().Apply(this);
			}

			OutAWhileCondiblck(node);
		}

		public virtual void InAMcallCondistmts(AMcallCondistmts node) {
			DefaultIn(node);
		}

		public virtual void OutAMcallCondistmts(AMcallCondistmts node) {
			DefaultOut(node);
		}

		public override void CaseAMcallCondistmts(AMcallCondistmts node) {
			InAMcallCondistmts(node);
			if (node.GetMcall() != null) {
				node.GetMcall().Apply(this);
			}

			if (node.GetCondistmts() != null) {
				node.GetCondistmts().Apply(this);
			}

			OutAMcallCondistmts(node);
		}

		public virtual void InAWifCondistmts(AWifCondistmts node) {
			DefaultIn(node);
		}

		public virtual void OutAWifCondistmts(AWifCondistmts node) {
			DefaultOut(node);
		}

		public override void CaseAWifCondistmts(AWifCondistmts node) {
			InAWifCondistmts(node);
			if (node.GetIf() != null) {
				node.GetIf().Apply(this);
			}

			if (node.GetCondistmts() != null) {
				node.GetCondistmts().Apply(this);
			}

			OutAWifCondistmts(node);
		}

		public virtual void InAOperationCondistmts(AOperationCondistmts node) {
			DefaultIn(node);
		}

		public virtual void OutAOperationCondistmts(AOperationCondistmts node) {
			DefaultOut(node);
		}

		public override void CaseAOperationCondistmts(AOperationCondistmts node) {
			InAOperationCondistmts(node);
			if (node.GetOperation() != null) {
				node.GetOperation().Apply(this);
			}

			if (node.GetCondistmts() != null) {
				node.GetCondistmts().Apply(this);
			}

			OutAOperationCondistmts(node);
		}

		public virtual void InADclCondistmts(ADclCondistmts node) {
			DefaultIn(node);
		}

		public virtual void OutADclCondistmts(ADclCondistmts node) {
			DefaultOut(node);
		}

		public override void CaseADclCondistmts(ADclCondistmts node) {
			InADclCondistmts(node);
			if (node.GetDcl() != null) {
				node.GetDcl().Apply(this);
			}

			if (node.GetCondistmts() != null) {
				node.GetCondistmts().Apply(this);
			}

			OutADclCondistmts(node);
		}

		public virtual void InAExtraCondistmts(AExtraCondistmts node) {
			DefaultIn(node);
		}

		public virtual void OutAExtraCondistmts(AExtraCondistmts node) {
			DefaultOut(node);
		}

		public override void CaseAExtraCondistmts(AExtraCondistmts node) {
			InAExtraCondistmts(node);
			if (node.GetCondistmts() != null) {
				node.GetCondistmts().Apply(this);
			}

			OutAExtraCondistmts(node);
		}

		public virtual void InABreakCondistmts(ABreakCondistmts node) {
			DefaultIn(node);
		}

		public virtual void OutABreakCondistmts(ABreakCondistmts node) {
			DefaultOut(node);
		}

		public override void CaseABreakCondistmts(ABreakCondistmts node) {
			InABreakCondistmts(node);
			OutABreakCondistmts(node);
		}

		public virtual void InAContinueCondistmts(AContinueCondistmts node) {
			DefaultIn(node);
		}

		public virtual void OutAContinueCondistmts(AContinueCondistmts node) {
			DefaultOut(node);
		}

		public override void CaseAContinueCondistmts(AContinueCondistmts node) {
			InAContinueCondistmts(node);
			OutAContinueCondistmts(node);
		}

		public virtual void InAEndofwhileCondistmts(AEndofwhileCondistmts node) {
			DefaultIn(node);
		}

		public virtual void OutAEndofwhileCondistmts(AEndofwhileCondistmts node) {
			DefaultOut(node);
		}

		public override void CaseAEndofwhileCondistmts(AEndofwhileCondistmts node) {
			InAEndofwhileCondistmts(node);
			OutAEndofwhileCondistmts(node);
		}

		public virtual void InACond(ACond node) {
			DefaultIn(node);
		}

		public virtual void OutACond(ACond node) {
			DefaultOut(node);
		}

		public override void CaseACond(ACond node) {
			InACond(node);
			if (node.GetLeft() != null) {
				node.GetLeft().Apply(this);
			}

			if (node.GetCompop() != null) {
				node.GetCompop().Apply(this);
			}

			if (node.GetRight() != null) {
				node.GetRight().Apply(this);
			}

			if (node.GetCond() != null) {
				node.GetCond().Apply(this);
			}

			OutACond(node);
		}

		public virtual void InABoolCond(ABoolCond node) {
			DefaultIn(node);
		}

		public virtual void OutABoolCond(ABoolCond node) {
			DefaultOut(node);
		}

		public override void CaseABoolCond(ABoolCond node) {
			InABoolCond(node);
			if (node.GetValue() != null) {
				node.GetValue().Apply(this);
			}

			if (node.GetCond() != null) {
				node.GetCond().Apply(this);
			}

			OutABoolCond(node);
		}

		public virtual void InAAndfolcondCond(AAndfolcondCond node) {
			DefaultIn(node);
		}

		public virtual void OutAAndfolcondCond(AAndfolcondCond node) {
			DefaultOut(node);
		}

		public override void CaseAAndfolcondCond(AAndfolcondCond node) {
			InAAndfolcondCond(node);
			if (node.GetCond() != null) {
				node.GetCond().Apply(this);
			}

			OutAAndfolcondCond(node);
		}

		public virtual void InAOrfolcondCond(AOrfolcondCond node) {
			DefaultIn(node);
		}

		public virtual void OutAOrfolcondCond(AOrfolcondCond node) {
			DefaultOut(node);
		}

		public override void CaseAOrfolcondCond(AOrfolcondCond node) {
			InAOrfolcondCond(node);
			if (node.GetCond() != null) {
				node.GetCond().Apply(this);
			}

			OutAOrfolcondCond(node);
		}

		public virtual void InAOpenScope(AOpenScope node) {
			DefaultIn(node);
		}

		public virtual void OutAOpenScope(AOpenScope node) {
			DefaultOut(node);
		}

		public override void CaseAOpenScope(AOpenScope node) {
			InAOpenScope(node);
			OutAOpenScope(node);
		}

		public virtual void InACloseScope(ACloseScope node) {
			DefaultIn(node);
		}

		public virtual void OutACloseScope(ACloseScope node) {
			DefaultOut(node);
		}

		public override void CaseACloseScope(ACloseScope node) {
			InACloseScope(node);
			OutACloseScope(node);
		}
	}
}
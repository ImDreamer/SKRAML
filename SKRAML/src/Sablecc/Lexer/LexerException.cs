using System;

namespace SKRAML.Sablecc.Lexer {
	public class LexerException : ApplicationException
	{
		public LexerException(string message) : base (message)
		{
		}
	}
}
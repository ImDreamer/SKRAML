using System.Collections;
using System.IO;

namespace SKRAML.Sablecc.Lexer {
	internal class PushbackReader {
		private TextReader reader;
		private Stack stack = new Stack ();


		internal PushbackReader (TextReader reader)
		{
			this.reader = reader;
		}

		internal int Peek ()
		{
			if ( stack.Count > 0 ) return (int)stack.Peek();
			return reader.Peek();
		}

		internal int Read ()
		{
			if ( stack.Count > 0 ) return (int)stack.Pop();
			return reader.Read();
		}

		internal void Unread (int v)
		{
			stack.Push (v);
		}
	}
}
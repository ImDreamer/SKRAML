using System.Collections.Generic;
using System.Linq;
using SKRAML.Exception;
using SKRAML.Models;
using SKRAML.Sablecc.Node;
using SKRAML.Sablecc.Tokens;

namespace SKRAML {
	/// <summary>
	/// A collection of helpful methods.
	/// </summary>
	internal static class Utilities {
		/// <summary>
		/// Gets the standard value for a given type.
		/// </summary>
		/// <param name="type">The type we want to get value of.</param>
		/// <returns>Returns the standard value of the type.</returns>
		public static PValue GetStandardValueFromType(PType type) {
			switch (type) {
				case ABoolType _:
					return new ABoolValue(new TBoolvalue("false"));
				case AIntType _:
					return new AIntValue(new TIntvalue("0"));
				case ACharType _:
					return new ACharValue(new TCharvalue("'\0'"));
				case AFloatType _:
					return new AFloatValue(new TFloatvalue("0.0"));
				case AStringType _:
					return new AStringValue(new TStringvalue(""));
				default:
					return null;
			}
		}

		/// <summary>
		/// Gets the type from the value.
		/// If the block isn't null, we use <see cref="GetSymbolRecursive"/> to get the type if it's a identifier value.
		/// If the block is null we just return a identifier type.
		/// </summary>
		/// <param name="value">The value to turn into a type.</param>
		/// <param name="_block">The block to use.</param>
		/// <returns>Returns a type matching the values type.</returns>
		/// <exception cref="SKRAMLException">Throws if unknown value.</exception>
		public static PType GetTypeFromValue(this PValue value, Block _block) {
			switch (value) {
				case AStringValue _:
					return new AStringType();
				case ACharValue _:
					return new ACharType();
				case AIntValue _:
					return new AIntType();
				case AFloatValue _:
					return new AFloatType();
				case ABoolValue _:
					return new ABoolType();
				case AIdentifierValue _:
					return _block == null
						? new AIdentifierType(new TIdentifier(value.ToString()))
						: GetSymbolRecursive(_block, new TIdentifier(value.ToString())).Type;
				case AArrayvalueValue _:
					return new AArrayType();
				default:
					throw new SKRAMLException("No value?!?!?");
			}
		}

		/// <summary>
		/// Turns a argument list into a list of declarations.
		/// </summary>
		/// <param name="_argslist">The arglist to transform.</param>
		/// <param name="currentBlock">The block to look in.</param>
		/// <returns>Returns the arguments as a list of declarations.</returns>
		public static IEnumerable<Declaration>
			ConvertArgsListToDeclarationList(this AArgslist _argslist, Block currentBlock) {
			var result = new List<Declaration>();
			if (_argslist == null) return result;
			while (_argslist.GetRight() != null) {
				var rightNode = (AArgArgslist) _argslist.GetRight();
				result.Add(new Declaration(rightNode.GetIdentifier(), rightNode.GetType(),
					GetStandardValueFromType(rightNode.GetType()), currentBlock));
				if (_argslist.GetLeft() == null) {
					break;
				}

				_argslist = (AArgslist) ((AExtraArgslist) _argslist.GetLeft()).GetArgslist();
			}

			return result;
		}

		/// <summary>
		/// Looks for a symbol with the supplied identifier, starting from the supplied block.
		/// If the symbol isn't found the method is called recursively with the parent of the block.
		/// </summary>
		/// <param name="block">The block to start searching in.</param>
		/// <param name="identifier">The identifier to look for.</param>
		/// <param name="_throwEx">Should the method throw if the symbol isn't found.</param>
		/// <returns>Returns the symbol if found, otherwise null if not throwing.</returns>
		/// <exception cref="SKRAMLException">Throws if the symbol isn't found and it's set to throw.</exception>
		public static Declaration GetSymbolRecursive(Block block, TIdentifier identifier, bool _throwEx = true) {
			Declaration result =
				block.Declarations.FirstOrDefault(declaration => declaration.Identifier.Text.Equals(identifier.Text));
			if (result != null) return result;
			switch (block.ParentBlock) {
				case null when _throwEx:
					throw new SKRAMLException($"The Dcl '{identifier.Text.Trim()}' wasn't not found!");
				case null:
					return null;
				default:
					return GetSymbolRecursive(block.ParentBlock, identifier, _throwEx);
			}
		}

		/**
		 * Gets the blockmodifier for a block
		 * Is always represented by a startletter: s followed by 0
		 * followed by the index of each innerscope inside innerscopes
		 */
		public static string BlockModifier(Block block) {
			return block.ParentBlock == null
				? "s0"
				: $"{BlockModifier(block.ParentBlock)}{block.ParentBlock.InnerBlocks.IndexOf(block).ToString()}";
		}
	}
}
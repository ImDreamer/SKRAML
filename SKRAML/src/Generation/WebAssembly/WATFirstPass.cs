using SKRAML.Models;
using SKRAML.Sablecc.Node;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Generation.WebAssembly {
	public sealed class WATFirstPass : CodeGenerator {
		/**
		 * Constructor
		 */
		public WATFirstPass(Method main, Class _global, ref int indent) : base(main, _global, ref indent) { }

		/**
		 * Emits the start of a WebAssembly module, followed by initialization of memory
		 * Followed by the heap-pointer followed by a global variable that has the length
		 * Of the latest multiint in it
		 */
		public override void CaseStart(Start node) {
			base.CaseStart(node);
			Emit("(module");
			Indentation++;
			Emit("(import \"js\" \"memory\" (memory 100))");
			Emit("(global $memoryoffset (import \"js\" \"memoryoffset\") (mut i32))");
			Emit("(global $length (import \"js\" \"length\") (mut i32))");
		}

		/**
		 * Declarations
		 */
		public override void CaseAOperation(AOperation node) {
			base.CaseAOperation(node);
			if (node.GetType() == null) return;
			Emit(WATUtility.GetVariableDeclaration(node.GetType(), node.GetIdentifier(), CurrentBlock, Indentation));
		}

		/**
		 * Declarations
		 */
		public override void CaseADcl(ADcl node) {
			base.CaseADcl(node);
			Emit(WATUtility.GetVariableDeclaration(node.GetType(), node.GetIdentifier(), CurrentBlock, Indentation));
		}

		/**
		 * Declarations
		 */
		public override void CaseAObjectOperation(AObjectOperation node) {
			base.CaseAObjectOperation(node);
			if (node.GetType() == null) return;
			Emit(WATUtility.GetVariableDeclaration(node.GetType(), node.GetName(), CurrentBlock, Indentation));
		}

		/**
		 * Declarations
		 */
		public override void CaseAArrayOperation(AArrayOperation node) {
			base.CaseAArrayOperation(node);
			if (node.GetType() == null) return;
			Emit(WATUtility.GetVariableDeclaration(node.GetType(), node.GetIdentifier(), CurrentBlock, Indentation));
		}

		/**
		 * Writes the code to the skraml.wat file
		 */
		public override void CaseEOF(EOF node) {
			base.CaseEOF(node);
			WriteCodeToFile("skraml/temp/skraml", "wat");
		}
	}
}
using System;
using System.Diagnostics;
using System.IO;
using SKRAML.Exception;
using SKRAML.Models;
using SKRAML.Sablecc.Node;

namespace SKRAML.Generation.WebAssembly {
	/// <summary>
	/// The Web Assembly Text format code generator passes.
	/// </summary>
	public sealed class WATCodeGenerator {
		/// <summary>
		/// Travers the tree with all the WAT passes, creates folders and generates the WASM.
		/// </summary>
		/// <param name="tree">The root node of the tree.</param>
		/// <param name="main">The main method.</param>
		/// <param name="global">The global block.</param>
		public WATCodeGenerator(Start tree, Method main, Class global) {
			var indent = 0;
			CodeGenerator.CheckAndCreateDirs();
			Clean();
			//First pass of WAT
			tree.Apply(new WATFirstPass(main, global, ref indent));
			//Middle passes of WAT
			//TODO
			//Last pass of WAT
			tree.Apply(new WATLastPass(main, global, ref indent));
			Console.WriteLine("Generated: temp/skraml.wat \uD83D\uDEAE");
			GenerateWasm();
		}

		/// <summary>
		/// Deletes all the files the skraml/bin and skraml/temp directories.
		/// </summary>
		private static void Clean() {
			foreach (FileInfo file in new DirectoryInfo("skraml/bin/").GetFiles()) {
				file.Delete();
			}

			foreach (FileInfo file in new DirectoryInfo("skraml/temp/").GetFiles()) {
				file.Delete();
			}
		}

		/// <summary>
		/// Gets the OS name.
		/// </summary>
		private static string OS = Environment.OSVersion.ToString().ToLower();

		/// <summary>
		/// Generates the WASM using WAT2WASM.
		/// </summary>
		/// <exception cref="SKRAMLException">Throws if the WAT2WASM encounters an error.</exception>
		private static void GenerateWasm() {
			string wat2WasmPath = $"lib/wat/{(OS.Contains("win") ? "wat2wasm.exe" : "wat2wasm")}";

			var start = new ProcessStartInfo {
				Arguments = "skraml/temp/skraml.wat -o skraml/bin/skraml.wasm",
				FileName = wat2WasmPath,
				WindowStyle = ProcessWindowStyle.Hidden,
				CreateNoWindow = true
			};

			using (Process proc = Process.Start(start)) {
				proc.WaitForExit();
				if (proc.ExitCode == 1)
					throw new SKRAMLException("Failed to gen wat code");
			}

			Console.WriteLine("Generated: bin/skraml.wasm \uD83D\uDEAE");
		}
	}
}
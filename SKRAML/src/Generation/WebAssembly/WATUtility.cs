using SKRAML.Exception;
using SKRAML.Models;
using SKRAML.Sablecc.Node;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Generation.WebAssembly {
    public class WATUtility {
        /**
         * Maps a type to singleint, float or multiint, depending on how it can be represented in WAT
         * Types that can currently be represented by a single integer: Integer, Bool, Char
         * Types that can currently be represented by a single float: Float
         * Types that can currently be represented indirectly by two integers: The rest
         */
        private static string VariableDclHelper(PType type) {
            switch (type) {
                case AIntType i:
                case ABoolType b:
                case ACharType c:
                    return "singleint";
                case AFloatType f:
                    return "float";
                default:
                    return "multiint";
            }
        }

        /**
         * Gets the string that declares the global variable for a certain type
         */
        public static string GetVariableDeclaration(PType type, TIdentifier ident, Block block, int indent) {
            switch (VariableDclHelper(type)) {
                case "singleint":
                    return "(global $" + ident.Text.Trim() + Utilities.BlockModifier(block) +
                            "(import \"js\" \"" + ident.Text.Trim() + Utilities.BlockModifier(block) + "\")(mut i64))";
                case "float":
                    return "(global $" + ident.Text.Trim() + Utilities.BlockModifier(block) +
                            "(import \"js\" \"" + ident.Text.Trim() + Utilities.BlockModifier(block) + "\")(mut f64))";
                case "multiint":
                    string tempString = "";
                    tempString += "(global $" + ident.Text.Trim() + "first" + Utilities.BlockModifier(block) +
                                  "(import \"js\" \"" + ident.Text.Trim() + "first" + Utilities.BlockModifier(block) + "\")(mut i64))";
                    for (int i = 0; i < indent; i++)
                        tempString += "\t";
                    tempString += "(global $" + ident.Text.Trim() + "second" + Utilities.BlockModifier(block) +
                                  "(import \"js\" \"" + ident.Text.Trim() + "second" + Utilities.BlockModifier(block) + "\")(mut i64))";
                    return tempString;
                default:
                    throw new SKRAMLException("Some type is not handled correctly");
            }
        }
    }
}
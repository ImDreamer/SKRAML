using SKRAML.Models;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Generation.WebAssembly {
	public sealed class WATLastPass : CodeGenerator {
		public WATLastPass(Method main, Class _global, ref int indent) : base(main, _global, ref indent) { }

		public override void CaseEOF(EOF node) {
			base.CaseEOF(node);
			Indentation--;
			Emit(")");
			WriteCodeToFile("skraml/temp/skraml", "wat");
		}
	}
}
using System.IO;
using System.Text;
using SKRAML.Analysis;
using SKRAML.Models;

namespace SKRAML.Generation {
	/// <summary>
	/// Code generator base class giving basic methods for emitting code.
	/// </summary>
	public abstract class CodeGenerator : BlockAdapter {
		private StringBuilder _code = new StringBuilder();
		protected int Indentation;

		protected CodeGenerator(Method main, Class _global, ref int indent) : base(main, _global) {
			Indentation = indent;
		}

		/// <summary>
		/// Creates the directories skraml/temp and skraml/bin.
		/// </summary>
		public static void CheckAndCreateDirs() {
			Directory.CreateDirectory("skraml/temp");
			Directory.CreateDirectory("skraml/bin");
		}

		/// <summary>
		/// Emits a line of code, with the correct indentation and a new line.
		/// </summary>
		/// <param name="code">The code to emit.</param>
		public void Emit(string code) {
			for (var i = 0; i < Indentation; i++)
				_code.Append("\t");
			_code.Append(code).Append("\n");
		}

		/// <summary>
		/// Emits a piece of code with the correct indentation, but without a new line.
		/// </summary>
		/// <param name="code">The code to emit.</param>
		protected void EmitWONS(string code) {
			for (var i = 0; i < Indentation; i++)
				_code.Append("\t");
			_code.Append(code);
		}

		/// <summary>
		/// Emits a piece of code without indentation and without a new line.
		/// </summary>
		/// <param name="code">The code to emit.</param>
		protected void EmitWON(string code) {
			_code.Append(code);
		}

		/// <summary>
		/// Writes the code to a file at the specified path with the extension.
		/// </summary>
		/// <param name="path">The path to the file.</param>
		/// <param name="extension">The extension of the file.</param>
		protected void WriteCodeToFile(string path, string extension) {
			CheckAndCreateDirs();
			File.AppendAllText($"{path}.{extension}", _code.ToString());
			_code = new StringBuilder();
		}
	}
}
using System.Collections.Generic;
using SKRAML.Exception;
using SKRAML.Models;
using SKRAML.Sablecc.Node;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Analysis {
	/// <summary>
	/// A pass discovering all binding occurrences. 
	/// </summary>
	public sealed class DeclarationAnalyser : BlockAdapter {
		/// <summary>
		/// The list of non bound occurrences.
		/// </summary>
		private readonly List<Declaration> _nonBoundDeclarations = new List<Declaration>();

		/// <summary>
		/// When a declarations is met we add it to the list of declarations in the block.
		/// </summary>
		/// <param name="node">The dcl node.</param>
		public override void InADcl(ADcl node) {
			base.InADcl(node);
			CurrentBlock.AddDeclaration(new Declaration(node.GetIdentifier(), node.GetType(),
				Utilities.GetStandardValueFromType(node.GetType()), CurrentBlock));
		}

		/// <summary>
		/// When a object is met, we check if it's a declaration and assigment and just add it to the declarations,
		/// otherwise if we can't find it in our current symbol table, we add it the non bound occurrences list
		/// </summary>
		/// <param name="node">The object operation node.</param>
		public override void InAObjectOperation(AObjectOperation node) {
			base.InAObjectOperation(node);
			if (node.GetType() == null) {
				if (GetSymbol(node.GetName(), false) == null) {
					_nonBoundDeclarations.Add(new Declaration(node.GetName(), node.GetType(),
						Utilities.GetStandardValueFromType(node.GetType()), CurrentBlock));
				}
			}
			else {
				CurrentBlock.AddDeclaration(new Declaration(node.GetName(), node.GetType(),
					Utilities.GetStandardValueFromType(node.GetType()), CurrentBlock));
			}
		}

//TODO: Comment
		/// <summary>
		/// When a operation is met we check if it's a single val op, if so we add it to list.
		/// If it's not a dcl assigment, then we look it up in the symbol table, if not found. We add it to non bound dcls.
		/// Otherwise we just add it to the list.
		/// </summary>
		/// <param name="node">The operation node.</param>
		public override void InAOperation(AOperation node) {
			base.InAOperation(node);
			if (node.GetOperation() is ASinglevalOperation singleVal && node.GetType() != null) {
				CurrentBlock.AddDeclaration(new Declaration(node.GetIdentifier(), node.GetType(),
					singleVal.GetValue(), CurrentBlock));
			}
			else if (node.GetType() == null) {
				if (GetSymbol(node.GetIdentifier(), false) == null) {
					_nonBoundDeclarations.Add(new Declaration(node.GetIdentifier(), node.GetType(),
						Utilities.GetStandardValueFromType(node.GetType()), CurrentBlock));
				}
			}
			else {
				CurrentBlock.AddDeclaration(new Declaration(node.GetIdentifier(), node.GetType(),
					Utilities.GetStandardValueFromType(node.GetType()), CurrentBlock));
			}
		}

		/// <summary>
		/// Checks if the array is declared correctly, and adds it the declaration list.
		/// </summary>
		/// <param name="node">The array node.</param>
		/// <exception cref="SKRAMLException">Throws a exception if the array is created wrongly.</exception>
		public override void InAArrayOperation(AArrayOperation node) {
			base.InAArrayOperation(node);
			if (!(node.GetType() is AArrayType))
				throw new SKRAMLException("Trying to create array with a non array type!");
			CurrentBlock.AddDeclaration(new Declaration(node.GetIdentifier(), node.GetType(), node.GetValue(),
				CurrentBlock));
		}

		/// <summary>
		/// Looks for the binding occurrences of the occurrences where at the time no bounding occurrences could be found.
		/// </summary>
		/// <param name="node">The eof node.</param>
		public override void CaseEOF(EOF node) {
			base.CaseEOF(node);
			foreach (Declaration nonBoundDeclaration in _nonBoundDeclarations) {
				Utilities.GetSymbolRecursive(nonBoundDeclaration.ParentBlock, nonBoundDeclaration.Identifier);
			}
		}
	}
}
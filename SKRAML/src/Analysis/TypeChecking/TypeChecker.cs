using SKRAML.Analysis.TypeChecking.Rules;
using SKRAML.Exception;
using SKRAML.Models;
using SKRAML.Sablecc.Node;

namespace SKRAML.Analysis.TypeChecking {
	/// <summary>
	/// Handles typechecking of the program.
	/// </summary>
	public sealed class TypeChecker : BlockAdapter {
		public TypeChecker(Method main, Class _global) : base(main, _global) { }

		public override void CaseAIfCondiblck(AIfCondiblck node) {
			base.CaseAIfCondiblck(node);
			if (!(node.GetCond() is ACond cond)) return;
			PType leftType = cond.GetLeft().GetTypeFromValue(CurrentBlock);
			PType rightType = cond.GetRight().GetTypeFromValue(CurrentBlock);

			var setup = new BinaryOperatorRule(rightType, leftType, cond.GetCompop().Text.Trim());
			BinaryOperatorRule rule = setup.Match();

			if (!(rule.ResultType is ABoolType)) {
				throw new SKRAMLException("Not a bool expression!");
			}
		}

		public override void InAOperation(AOperation node) {
			base.InAOperation(node);
			if (!(node.GetOperation() is AValsingOperation valOp)) return;
			PType leftType = ((ASinglevalOperation) valOp.GetLeft()).GetValue().GetTypeFromValue(CurrentBlock);
			PType rightType = ((ASinglevalOperation) valOp.GetRight()).GetValue().GetTypeFromValue(CurrentBlock);
			var setup = new BinaryOperatorRule(rightType, leftType, valOp.GetOp().Text.Trim());
			setup.Match();
		}
	}
}
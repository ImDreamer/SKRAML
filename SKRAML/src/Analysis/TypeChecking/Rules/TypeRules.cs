using System.Linq;
using SKRAML.Exception;
using SKRAML.Sablecc.Node;

namespace SKRAML.Analysis.TypeChecking.Rules {
	/// <summary>
	/// Data driven type rules.
	/// </summary>
	public static class TypeRules {
		/// <summary>
		/// Matches a BinaryOperatorRule with a rule defined in <see cref="BinaryOperatorRules"/>, if the rule isn't defined it throws an exception.
		/// </summary>
		/// <param name="setup">The input rule.</param>
		/// <returns>Returns the matching rule.</returns>
		/// <exception cref="SKRAMLException">Throws if no rule matches.</exception>
		public static BinaryOperatorRule Match(this BinaryOperatorRule setup) {
			BinaryOperatorRule rule = BinaryOperatorRules.FirstOrDefault(_rule => Equals(_rule, setup));
			if (rule == null) throw new SKRAMLException("Rule not found!");
			return rule;
		}

		/// <summary>
		/// Describes the type rules for binary operators.
		/// </summary>
		public static readonly BinaryOperatorRule[] BinaryOperatorRules = {
			new BinaryOperatorRule(new AIntType(), new AIntType(), "+", new AIntType()),
			new BinaryOperatorRule(new AIntType(), new AIntType(), "-", new AIntType()),
			new BinaryOperatorRule(new AIntType(), new AIntType(), "*", new AIntType()),
			new BinaryOperatorRule(new AIntType(), new AIntType(), "/", new AIntType()),
			new BinaryOperatorRule(new AIntType(), new AIntType(), "%", new AIntType()),
			new BinaryOperatorRule(new AIntType(), new AIntType(), "==", new ABoolType()),
			new BinaryOperatorRule(new AIntType(), new AIntType(), "!=", new ABoolType())
		};
	}
}
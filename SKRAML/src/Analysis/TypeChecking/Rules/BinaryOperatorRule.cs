using SKRAML.Sablecc.Node;

namespace SKRAML.Analysis.TypeChecking.Rules {
	/// <summary>
	/// A binary operation rule, describing each of the types.
	/// </summary>
	public sealed class BinaryOperatorRule {
		/// <summary>
		/// The right hand side of the operator type.
		/// </summary>
		public PType Right { get; }

		/// <summary>
		/// The left hand side of the operator.
		/// </summary>
		public PType Left { get; }

		/// <summary>
		/// The operation it self.
		/// </summary>
		public string Operation { get; }

		/// <summary>
		/// What the resulting type of the operation is.
		/// </summary>
		public PType ResultType { get; }

		/// <summary>
		/// Create a rule, this should be used to ask the type rules. Due to the resulting type aren't required.
		/// </summary>
		/// <param name="_right">The right hand side operators type.</param>
		/// <param name="_left">The left hand side operators type.</param>
		/// <param name="_operation">The operation.</param>
		public BinaryOperatorRule(PType _right, PType _left, string _operation) {
			Right = _right;
			Left = _left;
			Operation = _operation;
		}

		/// <summary>
		/// Creates a rule, the should be used when creating new type rules for the <see cref="TypeRules.BinaryOperatorRules"/>.
		/// </summary>
		/// <param name="_right">The right hand side operators type.</param>
		/// <param name="_left">The left hand side operators type.</param>
		/// <param name="_operation">The operation.</param>
		/// <param name="_resultType">The resulting type of the operation.</param>
		public BinaryOperatorRule(PType _right, PType _left, string _operation, PType _resultType) {
			Right = _right;
			Left = _left;
			Operation = _operation;
			ResultType = _resultType;
		}

		/// <summary>
		/// Defines the equality as if the right side and left side types are equal, and the operation is equal.
		/// If the result type is null for one of the objects. This isn't compared.
		/// </summary>
		/// <param name="other">The object to compare.</param>
		/// <returns>If the objects are equal.</returns>
		private bool Equals(BinaryOperatorRule other) {
			if (other.ResultType == null || ResultType == null)
				return Right.GetType() == other.Right.GetType() && Left.GetType() == other.Left.GetType() &&
				       string.Equals(Operation, other.Operation);
			return Right.GetType() == other.Right.GetType() && Left.GetType() == other.Left.GetType() &&
			       string.Equals(Operation, other.Operation) && ResultType.GetType() == other.ResultType.GetType();
		}

		/// <summary>
		/// Checks if two objects are equal. <see cref="Equals(SKRAML.Analysis.TypeChecking.Rules.BinaryOperatorRule)"/>
		/// </summary>
		/// <param name="obj">The object to compare.</param>
		/// <returns>Returns if the objects are equal.</returns>
		public override bool Equals(object obj) {
			return ReferenceEquals(this, obj) || obj is BinaryOperatorRule other && Equals(other);
		}

		/// <summary>
		/// Generates the hash code for the object.
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode() {
			unchecked {
				int hashCode = (Right != null ? Right.GetType().GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (Left != null ? Left.GetType().GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (Operation != null ? Operation.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (ResultType != null ? ResultType.GetType().GetHashCode() : 0);
				return hashCode;
			}
		}
	}
}
using System;
using System.Collections.Generic;
using SKRAML.Models;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Analysis {
	/// <summary>
	/// A pass checking for warnings.
	/// </summary>
	public sealed class WarningAnalyzer : BlockAdapter {
		/// <summary>
		/// A list of all the warnings.
		/// </summary>
		private readonly List<Warning> _warnings = new List<Warning>();

		public WarningAnalyzer(Method main, Class _global) : base(main, _global) { }

		/// <summary>
		/// Called when the EOF node is visited.
		/// This method checks if a main method has been defined.
		/// And prints the warnings.
		/// </summary>
		/// <param name="node">The EOF node</param>
		public override void CaseEOF(EOF node) {
			base.CaseEOF(node);
			if (MainMethod == null)
				_warnings.Add(new Warning(WarningType.MissingEntryPoint, node.Line, node.Pos, "No main method found."));
			PrintWarnings();
		}

		/// <summary>
		/// Call it's self recursively and checks if all declarations has been used.
		/// </summary>
		/// <param name="block">The block to look in.</param>
		private void CheckUsage(Block block) {
			foreach (Block innerBlock in block.InnerBlocks) {
				CheckUsage(innerBlock);
			}

			foreach (Declaration dcl in block.Declarations) {
				//If the declaration hasn't been used.
				if (!dcl.IsUsed)
					_warnings.Add(new Warning(WarningType.Unused, dcl.Identifier.Line, dcl.Identifier.Pos,
						"The variable '" + dcl.Identifier + "' inside " + dcl.ParentBlock.Identifier.Text +
						" is unused!"));
				//If the declaration can be moved inside anthoer scope to localize.
				if (dcl.Uses.Count == 1 && !dcl.Uses[0].Identifier.ToString()
					    .Equals(dcl.ParentBlock.Identifier.ToString()) &&
				    dcl.Uses[0].Identifier.Text.Trim().Length != 0) {
					_warnings.Add(new Warning(WarningType.Localize, dcl.Identifier.Line, dcl.Identifier.Pos,
						"The variable '" + dcl.Identifier.ToString().Trim() +
						"' can be moved inside the scope: '" +
						dcl.Uses[0].Identifier.Text + "' without any problems!"));
				}
			}
		}

		/// <summary>
		/// Print all the warnings, sorted after the warnings line number.
		/// </summary>
		private void PrintWarnings() {
			CheckUsage(Global);
			_warnings.Sort((_warning, _warning1) => _warning.Line - _warning1.Line);
			Console.WriteLine("Warning(s) found: " + _warnings.Count);
			foreach (Warning warning in _warnings) {
				Console.WriteLine("⚠️" + warning.Message);
			}
		}
	}
}
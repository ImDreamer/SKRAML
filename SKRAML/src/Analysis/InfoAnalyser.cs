using System;
using System.Collections.Generic;
using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Node;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Analysis {
	/// <summary>
	/// Just a small pass, which prints all the methods and classes in the program.
	/// </summary>
	public sealed class InfoAnalyser : DepthFirstAdapter {
		/// <summary>
		/// List of all the methods in the program.
		/// </summary>
		private readonly List<string> _methodIdentifiers = new List<string>();

		/// <summary>
		/// List of all the classes in the program.
		/// </summary>
		private readonly List<string> _classes = new List<string>();

		/// <summary>
		/// Is called when a Method node is visited.
		/// Adds the method to the methods list.
		/// </summary>
		/// <param name="node">The method node.</param>
		public override void CaseAMethod(AMethod node) {
			base.CaseAMethod(node);
			_methodIdentifiers.Add($"Name:\t {node.GetIdentifier()} Returns:\t {node.GetType()}");
		}

		/// <summary>
		/// Is called when a Class node is visited.
		/// Adds the class to the class list.
		/// </summary>
		/// <param name="node">The class node.</param>
		public override void CaseAClasses(AClasses node) {
			base.CaseAClasses(node);
			_classes.Add($"Class: \t{node.GetIdentifier()}");
		}

		/// <summary>
		/// Is called when a EOF node is visited.
		/// Calls print stats.
		/// </summary>
		/// <param name="node">The eof node.</param>
		public override void CaseEOF(EOF node) {
			base.CaseEOF(node);
			PrintStats();
		}

		/// <summary>
		/// Prints the methods and the classes of the program.
		/// </summary>
		private void PrintStats() {
			//Print all the MethodIdentifiers.
			if (_methodIdentifiers.Count != 0) {
				Console.WriteLine("---------- Methods ----------");
				_methodIdentifiers.ForEach(s => Console.WriteLine(s));
			}

			//Print all the operations if there are any.
			if (_classes.Count == 0) return;
			Console.WriteLine("---------- Classes ----------");
			_classes.ForEach(s => Console.WriteLine(s));
		}
	}
}
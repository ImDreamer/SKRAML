using System.Linq;
using SKRAML.Exception;
using SKRAML.Models;
using SKRAML.Sablecc.Analysis;
using SKRAML.Sablecc.Node;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Analysis {
	/// <summary>
	/// Keeps track of the blocks, main method and the global block..
	/// </summary>
	public abstract class BlockAdapter : DepthFirstAdapter {
		/// <summary>
		/// The current block.
		/// </summary>
		public Block CurrentBlock { get; private set; }

		/// <summary>
		/// The global block.
		/// </summary>
		public Class Global { get; private set; }

		/// <summary>
		/// The programs main method.
		/// </summary>
		public Method MainMethod { get; private set; }

		/// <summary>
		/// Create a new block adapter using the main method and global block, if they aren't null.
		/// </summary>
		/// <param name="_mainMethod">The main method of the program.</param>
		/// <param name="_global">The global block of the program.</param>
		public BlockAdapter(Method _mainMethod = null, Class _global = null) {
			if (_global != null)
				Global = _global;
			if (_mainMethod != null)
				MainMethod = _mainMethod;
		}

		/// <summary>
		/// Adds the block to the current blocks inner blocks and changes the block to the new block.
		/// If the block isn't already contained int the block.
		/// </summary>
		/// <param name="_block">The block to open.</param>
		private void OpenScope(Block _block) {
			if (!CurrentBlock.InnerBlocks.Any(_block1 =>
				_block.GetType() == _block1.GetType() && _block.Identifier.Text == _block1.Identifier.Text))
				CurrentBlock.AddBlock(_block);
			CurrentBlock = _block;
		}

		/// <summary>
		/// Closes the current block by setting the current block to the parent block.
		/// </summary>
		private void CloseScope() {
			CurrentBlock = CurrentBlock.ParentBlock;
		}

		/// <summary>
		/// Calls <see cref="Utilities.GetSymbolRecursive"/> with the current block.
		/// </summary>
		/// <param name="_identifier">The symbol identifier.</param>
		/// <param name="throwEx">If the method should throw an exception if the symbol isn't found.</param>
		/// <returns>Returns the symbol if it's found, otherwise if throwEx is false null is returned.</returns>
		public Declaration GetSymbol(TIdentifier _identifier, bool throwEx = true) {
			return Utilities.GetSymbolRecursive(CurrentBlock, _identifier, throwEx);
		}

		/// <summary>
		/// Creates a new global if it's null.
		/// And sets the current block to the global block.
		/// </summary>
		/// <param name="node">The start node.</param>
		public override void InAStart(AStart node) {
			base.InAStart(node);
			if (Global == null)
				Global = new Class(new TIdentifier("Global"), null);
			CurrentBlock = Global;
		}

		/// <summary>
		/// When a open scope node is met open a new block.
		/// </summary>
		/// <param name="node">The open scope node.</param>
		public override void InAOpenScope(AOpenScope node) {
			base.InAOpenScope(node);
			OpenScope(new Block(
				new TIdentifier("", CurrentBlock.Identifier.Line, CurrentBlock.Identifier.Pos),
				CurrentBlock));
		}

		/// <summary>
		/// Closes the current block, when a close scope node is met.
		/// </summary>
		/// <param name="node"></param>
		public override void InACloseScope(ACloseScope node) {
			base.InACloseScope(node);
			CloseScope();
		}

		/// <summary>
		/// Checks if the method we met is the main method, if so creates it, if it already exists throw exception.
		/// Otherwise it creates a normal method.
		/// </summary>
		/// <param name="node">The method node.</param>
		public override void InAMethod(AMethod node) {
			base.InAMethod(node);
			if (node.GetIdentifier().Text.Equals("Main") && node.GetType() is AVoidType &&
			    node.GetArgslist() == null) {
				var main = new Method(node.GetIdentifier(), CurrentBlock, node.GetType());
				if (MainMethod != null && MainMethod != main)
					throw new SKRAMLException("Main already defined!");
				MainMethod = main;
				return;
			}

			var method = new Method(node.GetIdentifier(), CurrentBlock, node.GetType());
			method.AddArgument(
				(node.GetArgslist() as AArgslist).ConvertArgsListToDeclarationList(CurrentBlock));
			OpenScope(method);
		}

		/// <summary>
		/// Closes the current block, when we leave a method.
		/// </summary>
		/// <param name="node">The method node.</param>
		public override void OutAMethod(AMethod node) {
			base.OutAMethod(node);
			CloseScope();
		}

		/// <summary>
		/// Opens a new block when a class is met.
		/// </summary>
		/// <param name="node">The class node.</param>
		public override void InAClasses(AClasses node) {
			base.InAClasses(node);
			OpenScope(new Class(node.GetIdentifier(), CurrentBlock));
		}

		/// <summary>
		/// Closes the current block when a class is exited.
		/// </summary>
		/// <param name="node">The class node.</param>
		public override void OutAClasses(AClasses node) {
			base.OutAClasses(node);
			CloseScope();
		}
	}
}
namespace SKRAML.Exception {
	public class SKRAMLException : System.Exception {
		public SKRAMLException(string message) : base(message) { }
	}
}
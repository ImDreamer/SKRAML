namespace SKRAML.Models {
	/// <summary>
	/// A warning.
	/// </summary>
	public struct Warning {
		/// <summary>
		/// The column position.
		/// </summary>
		public int Position { get; }

		/// <summary>
		/// The line number.
		/// </summary>
		public int Line { get; }

		/// <summary>
		/// The message.
		/// </summary>
		public string Message { get; }

		public Warning(WarningType type, int position, int line, string message) {
			Position = position;
			Line = line;
			Message = $"[{type.ToString()}] [{line.ToString()},{position.ToString()}] {message}";
		}
	}

	/// <summary>
	/// Warning types of the compiler.
	/// </summary>
	public enum WarningType {
		Unused,
		Localize,
		MissingEntryPoint
	}
}
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Models {
	/// <summary>
	/// A class block.
	/// </summary>
	public sealed class Class : Block {
		public Class(TIdentifier identifier, Block parentBlock) : base(identifier, parentBlock) { }
	}
}
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SKRAML.Exception;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Models {
	/// <summary>
	/// A block in the program (Can also be seen as a scope).
	/// </summary>
	public class Block {
		/// <summary>
		/// The identifier of the block.
		/// </summary>
		public TIdentifier Identifier { get; }

		/// <summary>
		/// The parent block.
		/// </summary>
		/// <remarks>
		/// Null if global block.
		/// </remarks>
		public Block ParentBlock { get; }

		/// <summary>
		/// Gives a readonly list of the inner blocks.
		/// </summary>
		public ReadOnlyCollection<Block> InnerBlocks => _innerBlocks.AsReadOnly();

		/// <summary>
		/// Gives a readonly list of the declarations.
		/// </summary>
		public ReadOnlyCollection<Declaration> Declarations => _declarations.AsReadOnly();

		/// <summary>
		/// The declarations of the block.
		/// </summary>
		private readonly List<Declaration> _declarations = new List<Declaration>();

		/// <summary>
		/// The inner blocks of the block.
		/// </summary>
		private readonly List<Block> _innerBlocks = new List<Block>();

		public Block(TIdentifier identifier, Block parentBlock) {
			Identifier = identifier;
			ParentBlock = parentBlock;
		}

		/// <summary>
		/// Adds a block, and does checks depending on the type <see cref="CheckClass"/> and <see cref="CheckMethod"/>.
		/// </summary>
		/// <param name="innerBlock">The block to add.</param>
		public void AddBlock(Block innerBlock) {
			switch (innerBlock) {
				case Class _:
					CheckClass(innerBlock);
					break;
				case Method _:
					CheckMethod(innerBlock);
					break;
			}

			_innerBlocks.Add(innerBlock);
		}

		/// <summary>
		/// Checks if the method has been defined and if the methods isn't declared inside a class.
		/// </summary>
		/// <param name="methodBlock">The method block.</param>
		/// <exception cref="SKRAMLException">Throw if we are trying to do nested methods or it's already defined.</exception>
		private void CheckMethod(Block methodBlock) {
			if (!(methodBlock.ParentBlock is Class))
				throw new SKRAMLException("Nested methods aren't allowed!");
			if (_innerBlocks.Any(block => block.Identifier.Text == methodBlock.Identifier.Text))
				throw new SKRAMLException("Method already defined!");
		}

		/// <summary>
		/// Checks if the class has been defined and if the class isn't declared inside a global.
		/// </summary>
		/// <param name="classBlock">The class block.</param>
		/// <exception cref="SKRAMLException">Throw if we are trying to do nested classes or it's already defined.</exception>
		private void CheckClass(Block classBlock) {
			if (ParentBlock != null)
				throw new SKRAMLException("Nested class's aren't allowed!");
			if (_innerBlocks.Any(block => block.Identifier.Text == classBlock.Identifier.Text))
				throw new SKRAMLException("Class already defined!");
		}

		/// <summary>
		/// Adds a declaration if it isn't already defined.
		/// </summary>
		/// <param name="declaration">The declaration to add.</param>
		/// <exception cref="SKRAMLException">Throw if the declaration already exists.</exception>
		public void AddDeclaration(Declaration declaration) {
			if (_declarations.Any(dcl => dcl.Identifier.Line == declaration.Identifier.Line))
				throw new SKRAMLException("Already defined!");
			_declarations.Add(declaration);
		}
	}
}
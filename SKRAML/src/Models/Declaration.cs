using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SKRAML.Sablecc.Node;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Models {
	/// <summary>
	/// A declaration containing inforation about the declaration.
	/// </summary>
	public sealed class Declaration {
		/// <summary>
		/// The blocks where this declaration is used.
		/// </summary>
		private readonly HashSet<Block> _uses = new HashSet<Block>();

		/// <summary>
		/// The declarations identifier.
		/// </summary>
		public TIdentifier Identifier { get; }

		/// <summary>
		/// The declarations type.
		/// </summary>
		public PType Type { get; }

		/// <summary>
		/// The declarations value.
		/// </summary>
		public PValue Value { get; }

		/// <summary>
		/// Returns a readonly list of the uses of the declaration.
		/// </summary>
		public ReadOnlyCollection<Block> Uses => _uses.ToList().AsReadOnly();

		/// <summary>
		/// The declarations parent block.
		/// </summary>
		public Block ParentBlock { get; }

		/// <summary>
		/// True if used anywhere.
		/// </summary>
		public bool IsUsed => Uses.Count != 0;

		public Declaration(TIdentifier identifier, PType type, PValue value, Block parentBlock) {
			Identifier = identifier ?? throw new ArgumentNullException(nameof(identifier));
			Type = type;
			Value = value;
			ParentBlock = parentBlock ?? throw new ArgumentNullException(nameof(parentBlock));
		}

		/// <summary>
		/// Adds a usage if not already contained.
		/// </summary>
		/// <param name="block"></param>
		public void AddUsage(Block block) {
			if (_uses.Contains(block))
				_uses.Add(block);
		}

		/// <summary>
		/// Defines equality for declarations.
		/// </summary>
		/// <param name="other">The other declaration.</param>
		/// <returns>Returns true if equal.</returns>
		private bool Equals(Declaration other) {
			return Identifier.Equals(other.Identifier) && Type.Equals(other.Type) && Value.Equals(other.Value) &&
			       Uses.Equals(other.Uses) && ParentBlock.Equals(other.ParentBlock);
		}

		/// <summary>
		/// Defines equality for declarations.
		/// </summary>
		/// <param name="other">The other declaration.</param>
		/// <returns>Returns true if equal.</returns>
		public override bool Equals(object obj) {
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			return obj.GetType() == GetType() && Equals((Declaration) obj);
		}

		/// <summary>
		/// Generates the hash code for the object.
		/// </summary>
		/// <returns>The hash code.</returns>
		public override int GetHashCode() {
			unchecked {
				int hashCode = Identifier.GetHashCode();
				hashCode = (hashCode * 397) ^ Type.GetHashCode();
				hashCode = (hashCode * 397) ^ Value.GetHashCode();
				hashCode = (hashCode * 397) ^ Uses.GetHashCode();
				hashCode = (hashCode * 397) ^ ParentBlock.GetHashCode();
				return hashCode;
			}
		}
	}
}
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SKRAML.Exception;
using SKRAML.Sablecc.Node;
using SKRAML.Sablecc.Tokens;

namespace SKRAML.Models {
	/// <summary>
	/// A method block, containing the arguments of the method and the return type.
	/// </summary>
	public sealed class Method : Block {
		/// <summary>
		/// The arguments of the method.
		/// </summary>
		private readonly List<Declaration> _arguments = new List<Declaration>();

		/// <summary>
		/// The methods return type.
		/// </summary>
		public PType ReturnType { get; }

		/// <summary>
		/// Returns a readonly list of the arguments.
		/// </summary>
		public ReadOnlyCollection<Declaration> Arguments => _arguments.AsReadOnly();

		public Method(TIdentifier identifier, Block parentBlock, PType returnType) : base(identifier, parentBlock) {
			ReturnType = returnType;
		}

		/// <summary>
		/// Adds a single argument to the list, if not already defined.
		/// </summary>
		/// <param name="declaration">The declaration to add.</param>
		/// <exception cref="SKRAMLException">Throws if a argument with the same name is defined.</exception>
		public void AddArgument(Declaration declaration) {
			if (_arguments.Any(dcl => dcl.Identifier.Text == declaration.Identifier.Text))
				throw new SKRAMLException("Argument already defined!");
			_arguments.Add(declaration);
		}

		/// <summary>
		/// Adds a collection if arguments to the list <see cref="AddArgument(SKRAML.Models.Declaration)"/>
		/// </summary>
		/// <param name="declarations">The collection of arguments.</param>
		public void AddArgument(IEnumerable<Declaration> declarations) {
			foreach (Declaration dcl in declarations) {
				AddArgument(dcl);
			}
		}
	}
}
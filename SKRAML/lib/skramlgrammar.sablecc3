Package skraml.sablecc;
Helpers 		
	digit 			= ['0' .. '9'] | '-'['0' .. '9'];
	unsigneddig		= ['0' .. '9'];
	fractional  	= (digit)+'.'(unsigneddig)+ ;
	cr 				= 13;
    lf 				= 10;
	cr_lf			= [cr + lf];
	qm				= '"';
	letter 			= ['a' .. 'z'] | ['A' .. 'Z'];
    all 			= [0 .. 0xFFFF];
	not_cr_lf 		= [all - [cr + lf]];
	not_cr_lf_qm 	= [all - [cr_lf + qm]];
    not_star 		= [all - '*'];
    not_star_slash 	= [not_star - '/'];
	tab 			= 9;   
    eol 			= cr lf | cr | lf; // This takes care of different platforms
	blank 			= (' ' | tab | eol)+;
	true			= 'true';
	false			= 'false';
	intliteral		= (digit)+;
	charliteral 	= '''all''';
	floatliteral 	= fractional;
	boolliteral 	= 'true' | 'false';
	stringliteral 	= '"'(not_cr_lf_qm)*'"';
	short_comment 	= '//' not_cr_lf* eol;
    long_comment 	= '/*' not_star* '*'+ (not_star_slash not_star* '*'+)* '/';
    comment 		= short_comment 
					| long_comment;
					
Tokens
	boolkeyword		= 'bool';
	charkeyword		= 'char';
	floatkeyword	= 'float';
	intkeyword		= 'int';
	stringkeyword	= 'string';
	voidkeyword		= 'void';
					
	boolvalue 		= boolliteral;
	intvalue 		= intliteral;
	charvalue 		= charliteral;
	floatvalue		= floatliteral;
	stringvalue		= stringliteral;
	
	l_paren 		= '(';
	r_paren 		= ')';
	l_curly 		= '{';
	r_curly 		= '}';
	l_square		= '[';
	r_square		= ']';
	dot				= '.';
	
	backslash       = '\';

	classl			= 'class';
	return			= 'return';
	newl			= 'new';
	semicol			= ';';
	equals			= '=';
	op				= '+' | '-';
	upperops		= '%' | '*' | '/';
	ifl				= 'if';
	elsifl			= 'else if';
	elsel			= 'else';
	whilel			= 'while';
	break			= 'break';
	continue		= 'continue';
	compop			= '!=' | '<' | '<=' | '==' | '>' | '>=';
	ampsand			= '&';
	pipe			= '|';
	blank 			= blank;
	comma 			= ',';
	identifier 		= letter (letter | digit)*;
	comment 		= comment;
	
Ignored Tokens
    blank,
	comment;

Productions

	start {->start}				= program																		{-> New start(program.start)};																		
	
	program {->start}			= startstmts																	{-> New start.program(startstmts.start)}
								| {eof}																			{-> New start.eof()};
	
	startstmts {->start}		= {dcls} 	operation semicol program											{-> New start.dcls(operation, program.start)}
								| {methods}	method program														{-> New start.methods(method, program.start)}
								| {classes}	classes program														{-> New start.classes(classes, program.start)}
								| {dcl}		dcl semicol program 												{-> New start.dcl(dcl, program.start)};
								
		
	classes {->classes}			= classl identifier l_curly startstmts? r_curly									{-> New classes(identifier, startstmts.start)};
	
	method {->method}			= returntype identifier l_paren argslist? r_paren l_curly block r_curly			{-> New method(returntype.type, identifier, argslist, block)};
	
	argslist {->argslist}		= extraargs? arg																{-> New argslist(extraargs.argslist, arg.argslist)};
	
	extraargs {->argslist}		= argslist comma																{-> New argslist.extra(argslist.argslist)};
	
	arg {->argslist}			= type identifier																{-> New argslist.arg(type, identifier)};
	
	types {->type}				= {identifier}	identifier														{-> New type.identifier(identifier)}
								| {bool} 		boolkeyword 													{-> New type.bool(boolkeyword)}
								| {char} 		charkeyword 													{-> New type.char(charkeyword)}
								| {float} 		floatkeyword													{-> New type.float(floatkeyword)}
								| {int} 		intkeyword  													{-> New type.int(intkeyword)}
								| {string} 		stringkeyword 													{-> New type.string(stringkeyword)};
								
	
	type {->type}				= types																			{-> types.type}
								| {array}		types l_square r_square											{-> New type.array(types.type)};
	
	value {->value}				= {identifier} 	identifier														{-> New value.identifier(identifier)}
								| {bool}		boolvalue														{-> New value.bool(boolvalue)}
								| {int}			intvalue														{-> New value.int(intvalue)}
								| {char}		charvalue														{-> New value.char(charvalue)}
								| {float}		floatvalue														{-> New value.float(floatvalue)}
								| {string}		stringvalue														{-> New value.string(stringvalue)}
								| {arrayvalue}	identifier l_square arrayinnervalue r_square					{-> New value.arrayvalue(identifier, arrayinnervalue.value)};
								
	
	arrayinnervalue {->value}	= {intvalue}	intvalue														{-> New value.arrayvalueint(intvalue)}
								| {identifier}	identifier														{-> New value.arrayvalueident(identifier)};
	
	returntype {->type}			= {normaltype} 	type 															{-> type.type}
								| {void} 		voidkeyword 													{-> New type.void(voidkeyword)}
								| {operation}	operation														{-> New type.operation(operation.operation)};
	
	block {->block}				= stmts? P.return?																{-> New block(stmts.block, P.return.block)};
	
	return {->block}			= T.return P.value? semicol														{-> New block.return(P.value)};

	stmts {->block}				= {stmt} stmts? stmt															{-> New block.stmts(stmts.block, stmt.stmt)}
								| {method} stmts? method														{-> New block.method(stmts.block, method)}
								| {op} stmts? operation	semicol													{-> New block.operation(stmts.block, operation.operation)}
								| {dcl} stmts? dcl semicol														{-> New block.dcl(stmts.block, dcl)};
									
	stmt {->stmt}				= {expr} methodcall semicol														{-> New stmt.methcall(methodcall.mcall)}
								| {identcall} identcall semicol													{-> New stmt.identcall(identcall.mcall)}
								| {condi} condistmts															{-> New stmt.condi(condistmts.condiblck)};
	
	identcall {->mcall}			= {ident} identifier firstfollowmcall											{-> New mcall.icall(identifier, firstfollowmcall.mcall)};
	
	methodcall {->mcall}		= identifier l_paren argscalllist? r_paren followmethodcall?					{-> New mcall(identifier, argscalllist.aclist, followmethodcall.mcall)};
	
	firstfollowmcall {->mcall}	= {ident} followicall															{-> followicall.mcall}
								| {method} dot methodcall														{-> methodcall.mcall};
								
	followicall {->mcall}		= dot identifier followmethodcall?												{-> New mcall.ident(identifier, followmethodcall.mcall)};
	
	followmethodcall {->mcall}	= {identifier} followicall														{-> followicall.mcall}
								| {methodcall} dot methodcall													{-> methodcall.mcall};
								
	argscalllist {->aclist}		= extraargslist? argscall														{-> New aclist(extraargslist.aclist, argscall.aclist)};
	
	extraargslist {->aclist}	= argscalllist comma															{-> New aclist.extra(argscalllist.aclist)};
	
	argscall {->aclist}			= P.value																		{-> New aclist.arg(P.value)}
								| {methodcall} methodcall														{-> New aclist.argmcall(methodcall.mcall)}
								| {identcall} identcall															{-> New aclist.argmcall(identcall.mcall)};

	dcl {->dcl}					= type identifier																{-> New dcl(type, identifier)};
	
	operation {->operation}		= {low} type? identifier equals valuerecursive									{-> New operation(type, identifier, valuerecursive.operation)}
								| {upper} type? identifier equals valueupperrec									{-> New operation(type, identifier, valueupperrec.operation)}
								| {object} type? [name]:identifier equals newl									{-> New operation.object(type, name)}
								| {arrayobj} [name]:identifier l_square arrayinnervalue r_square equals newl	{-> New operation.arrayobj(name, arrayinnervalue.value)}
								| {array} type? [name]:identifier equals newl l_square arrayinnervalue r_square {-> New operation.array(type, name, arrayinnervalue.value)}
								| {arrayassignlow} [name]:identifier l_square arrayinnervalue r_square equals valuerecursive {-> New operation.arrayass(name, arrayinnervalue.value, valuerecursive.operation)}
								| {arrayassignup} [name]:identifier l_square arrayinnervalue r_square equals valueupperrec {-> New operation.arrayass(name, arrayinnervalue.value, valueupperrec.operation)}
								| {mlow} methodcall equals valuerecursive										{-> New operation.mass(methodcall.mcall, valuerecursive.operation)}
								| {mhigh} methodcall equals valueupperrec										{-> New operation.mass(methodcall.mcall, valueupperrec.operation)}
								| {ilow} identcall equals valuerecursive										{-> New operation.mass(identcall.mcall, valuerecursive.operation)}
								| {ihigh} identcall equals valueupperrec										{-> New operation.mass(identcall.mcall, valueupperrec.operation)};
	
	valuerecursive {->operation}= {singular} [left]:valueupperrec op [right]:valueupperrec						{-> New operation.valsing(left.operation, op, right.operation)}
								| {multiple} valuerecursive op valueupperrec 									{-> New operation.valrecursive(valuerecursive.operation, op, valueupperrec.operation)};
								
	valueupperrec {->operation}	= {operation} valueupperrec upperops P.value									{-> New operation.valurec(valueupperrec.operation, upperops, P.value)}
								| {singlevalue} P.value															{-> New operation.singleval(P.value)}
								| {parenthesis} valuehighestrec													{-> valuehighestrec.operation}
								| {methodcall} methodcall														{-> New operation.methodcall(methodcall.mcall)}
								| {identcall} identcall															{-> New operation.methodcall(identcall.mcall)};
								
	valuehighestrec {->operation}	= {lowerrec} l_paren valuerecursive r_paren									{-> New operation.valhrec(valuerecursive.operation)}
								| {upperrec} l_paren valueupperrec r_paren										{-> New operation.valhrec(valueupperrec.operation)};
								
	condistmts {->condiblck}	= {if}if																		{-> if.condiblck}
								| {while}while																	{-> while.condiblck};
								
	if {->condiblck}			= ifl l_paren condition r_paren l_curly block loopmod? r_curly elsif? else?		{-> New condiblck.if(New scope.open(), condition.cond, block, loopmod.condistmts, New scope.close(), elsif.condiblck, else.condiblck)};
	
	elsif {->condiblck}			= elsifl l_paren condition r_paren loopmod? l_curly block r_curly elsif? 		{-> New condiblck.elsif(New scope.open(), condition.cond, block, loopmod.condistmts, New scope.close(), elsif.condiblck)};
	
	else {->condiblck}			= elsel l_curly block loopmod? r_curly											{-> New condiblck.else(New scope.open(), block, loopmod.condistmts, New scope.close())};
	
	while {->condiblck}			= whilel l_paren condition r_paren l_curly whileextra? r_curly					{-> New condiblck.while(New scope.open(), condition.cond, whileextra.condistmts, New scope.close())};
	
	whileblock {->condistmts}	= whileextra																	{-> New condistmts.extra(whileextra.condistmts)}
								| {endofwhile}																	{-> New condistmts.endofwhile()};
								
	whileextra {->condistmts}	= {mcall} 		methodcall semicol whileblock									{-> New condistmts.mcall(methodcall.mcall, whileblock.condistmts)}
								| {if}			if whileblock													{-> New condistmts.wif(if.condiblck, whileblock.condistmts)}
								| {operation}	operation semicol whileblock									{-> New condistmts.operation(operation, whileblock.condistmts)}
								| {dcl}			dcl semicol whileblock											{-> New condistmts.dcl(dcl, whileblock.condistmts)};
	
	loopmod {->condistmts}		= {break} break semicol															{-> New condistmts.break()}
								| {continue} continue semicol													{-> New condistmts.continue()};
								
	condition {->cond}			= {comparison} [left]:P.value compop [right]:P.value followcondition?			{-> New cond(left.value, compop, right.value, followcondition.cond)}
								| {bool} [bool]:P.value followcondition?										{-> New cond.bool(bool.value, followcondition.cond)};
	
	followcondition {->cond}	= {and}[first]:ampsand [second]:ampsand condition								{-> New cond.andfolcond(condition.cond)}
								| {or} [first]:pipe [second]:pipe condition										{-> New cond.orfolcond(condition.cond)};
	
Abstract Syntax Tree

	start		= 			 		start
				| {program}			start
				| {dcls}			operation start
				| {methods}			method start
				| {classes}			classes start
				| {dcl}				dcl start
				| {eof}				;
				
	classes		=					identifier start?;
	
	method		= 		 			type identifier argslist? block;
	
	argslist	= 					[left]:argslist? [right]: argslist
				| {extra} 			argslist
				| {arg} 			type identifier;
	
	type		= {identifier}		identifier
				| {bool} 			boolkeyword
				| {char} 			charkeyword
				| {float} 			floatkeyword
				| {int} 			intkeyword
				| {string} 			stringkeyword
				| {void} 			voidkeyword
				| {operation}		operation
				| {array}			type;
				
	value		= {identifier}		identifier
				| {bool} 			boolvalue 	
				| {int}				intvalue 	
				| {char}			charvalue 	
				| {float}			floatvalue	
				| {string}			stringvalue
				| {arrayvalue}		identifier value
				| {arrayvalueint}	intvalue
				| {arrayvalueident}	identifier;
				
	block		= 					[left]:block? [right]:block?
				| {return}			value?
				| {method}			block? method
				| {stmts} 			block? stmt
				| {operation}		block? operation
				| {dcl}				block? dcl;
				
	stmt		= {methcall} 		mcall
				| {identcall}		mcall
				| {condi}			condiblck;
				
	mcall		= 		 			identifier aclist? mcall?
				| {ident}			identifier mcall?
				| {icall}			identifier mcall;
	
	aclist		= 					[left]:aclist? [right]:aclist
				| {extra}			aclist
				| {arg}				P.value
				| {argmcall}		mcall;
	
	dcl			=	 				type identifier;
	
	operation	= 		 			type? identifier operation
				| {mass}			mcall operation
				| {valsing}			[left]:operation op [right]:operation
				| {valrecursive}	[left]:operation op [right]:operation
				| {valupsing}		operation
				| {valurec}			operation upperops P.value
				| {array}			type? identifier value
				| {arrayobj}		identifier value
				| {arrayass}		identifier value operation
				| {singleval}		P.value
				| {methodcall}		mcall
				| {valhrec}			operation
				| {object}			type? [name]:identifier;
	
	condiblck	= {if}				[open]:scope cond block [loopmod]:condistmts? [close]:scope [elsif]:condiblck? [else]:condiblck?
				| {elsif}			[open]:scope cond block [loopmod]:condistmts? [close]:scope [elsif]:condiblck?
				| {else}			[open]:scope block [loopmod]:condistmts? [close]:scope
				| {while}			[open]:scope cond condistmts? [close]:scope;
	
	condistmts	= {mcall}			mcall condistmts
				| {wif}				[if]:condiblck condistmts
				| {operation}		operation condistmts
				| {dcl}				dcl condistmts
				| {extra}			condistmts
				| {break}
				| {continue}
				| {endofwhile};
	
	cond		= 					[left]:value compop [right]:value cond?
				| {bool}			value cond?
				| {andfolcond}		cond
				| {orfolcond} 		cond;
				
	scope		= {open}
				| {close};

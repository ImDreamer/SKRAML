﻿using System;
using System.Collections.Generic;
using System.IO;
using SKRAML.Analysis;
using SKRAML.Analysis.TypeChecking;
using SKRAML.Generation.WebAssembly;
using SKRAML.Sablecc.Lexer;
using SKRAML.Sablecc.Node;
using SKRAML.Sablecc.Parser;

namespace SKRAML {
	internal static class Program {
		private static void Main(string[] args) {
			var arguments = new List<string>(args);
			Console.WriteLine("SKRAML Compiler️ \"WASM\" (WAT) SKRAML GROUP™ (C# edition)");
			Console.WriteLine("Press (Win CTRL + Z) (Linux CTRL + D) to EOF or CTRL + C to Exit");
			Parser parser;
			if (arguments.Contains("-f") && arguments.Count >= arguments.IndexOf("-f") + 1)
				parser = new Parser(
					new Lexer(new StringReader(File.ReadAllText(arguments[arguments.IndexOf("-f") + 1]))));
			else
				parser = new Parser(new Lexer(
#if !DEBUG
					Console.In
#else
					new StringReader(
						@"int Hello(){
						if(2 == 2){
						}
					} 
					int World(int a, float b){
					} 
					a = 2 + 2;
					class HelloWorld{
						int Hello(){
							if(2 == 2){
							}
						} 
						int World(int a, float b){
						}
					}
					int a = 0;")
#endif
				));

			//Parse the input in the reader.
			Start tree = parser.Parse();
			Console.WriteLine("Finished parsing ✔️");
			tree.Apply(new InfoAnalyser());
			Console.WriteLine("Finished info analyzer ✔️");
			var declarationAnalyser = new DeclarationAnalyser();
			tree.Apply(declarationAnalyser);
			Console.WriteLine("Finished declaration analyzer ✔️");
			var typeChecking = new TypeChecker(declarationAnalyser.MainMethod, declarationAnalyser.Global);
			tree.Apply(typeChecking);
			Console.WriteLine("Finished typechecking ✔️");
			tree.Apply(new WarningAnalyzer(typeChecking.MainMethod, typeChecking.Global));
			new WATCodeGenerator(tree, typeChecking.MainMethod, typeChecking.Global);
			Console.WriteLine("Finished code generation ✔️");
		}
	}
}